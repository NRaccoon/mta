local gates = {}
local mysql = exports.nx_mysql
local tesztKocsi = createVehicle(562, 1784.80048, 832.47809, 10.80644, 0, 0, 180)
local tesztKocsi = createVehicle(561, 1788.80048, 832.47809, 10.80644, 0, 0, 180)

function createBorders ()
	for k, v in pairs(bordersPos) do
        createObject(unpack(v))
    end
    for k, v in pairs(gatesPos) do
        local gate = createObject(988, unpack(v))
        table.insert(gates, gate)
    end
end
addEventHandler ( "onResourceStart", root, createBorders )

function gateOpener (index)
    local player = client
    if (getPlayerMoney(player) or 0) >= 500 then
        dbExec(mysql:getConnection(), "INSERT INTO `border` SET `kocsi` = ?, `player` = ?", getVehicleName(getPedOccupiedVehicle(player)), getPlayerName(player));
        takePlayerMoney(player, 500)
	    outputChatBox (getPlayerName(player) ..", átkelhet a határon.")
        local gate = gates[index]
        local x,y,z = gate:getPosition()
        local pos = gate.matrix:transformPosition(Vector3(-5, 0, 0))
        moveObject(gate, 1000, pos.x, pos.y, pos.z)

        setTimer( function()
            moveObject(gate, 1000, x,y,z)
            setTimer( function () triggerClientEvent("onHatarDialogShow", player) end, 1000, 1)
        end, 3000, 1);
    else
        outputChatBox (getPlayerName(client) .." egy csóró csicska gyász")
    end
end
addEvent("onGateOpen", true)
addEventHandler("onGateOpen", root, gateOpener)
