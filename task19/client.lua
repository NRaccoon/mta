local gateCols = {}
local screenWidth, screenHeight = guiGetScreenSize ( )
local fontName = dxCreateFont("hatar.ttf", 20)
local fontSize = 1
local text = "A határon való átkeléshez fizess 500 dollárt!"
local textWidth = dxGetTextWidth ( text, fontSize, fontName )
local x = screenWidth / 2 - textWidth / 2
local y = screenHeight / 2
local show = false
local button = guiCreateButton(0.48, 0.55, 0.05, 0.05, "Kifizet", true)
guiSetVisible(button, false)
local currentCol = nil


function playerPressedKey(button, press)
    if (press) and button == 'm' then
        showCursor ( not isCursorShowing ( ) )
    end
    if (press) and button == "enter" and show then
        letMeIn()
        cancelEvent()
    end
end
addEventHandler("onClientKey", root, playerPressedKey)


function hatarEnter (theElement, matchingDimension)
    if getElementType(theElement) == "player" then
        currentCol = source
    end
    if getElementType(theElement) == "vehicle" and getVehicleOccupant(theElement) == localPlayer and gateCols[source] and matchingDimension then
        hatarDialogShow()
    end
end
addEventHandler ("onClientColShapeHit", root, hatarEnter)

function hatarLeft (theElement, matchingDimension)
    if getElementType(theElement) == "player" then
        currentCol = nil
    end
    if gateCols[source] and matchingDimension then
        hatarDialogHide()
    end
end
addEventHandler ("onClientColShapeLeave", root, hatarLeft)

function createBorders ()
    for k, v in pairs(gatesPos) do
        local valami = Matrix(unpack(v))
        local pos = valami:transformPosition(Vector3(0, -5, 0))
        local col = createColSphere(pos.x, pos.y, pos.z, 5)
        gateCols[col] = k
    end
end
addEventHandler("onClientResourceStart", root, createBorders )

function hatarDialogShow()
    if getElementType(source)=="player" and not currentCol then return end
    if not isPedInVehicle(localPlayer) then return end
    show = true
    guiSetVisible(button, show)
    if show then
        addEventHandler ( "onClientRender", root, createText )
    end
end
addEvent("onHatarDialogShow", true)
addEventHandler("onHatarDialogShow", root, hatarDialogShow)

function hatarDialogHide()
    if show then
        removeEventHandler("onClientRender", root, createText)
    end
    show = false
    guiSetVisible(button, show)
end
addEvent("onHatarDialogHide", true)
addEventHandler("onHatarDialogHide", root, hatarDialogHide)

function createText ( )
    dxDrawRectangle ( x - 10, y - 10, textWidth + 20, 40, tocolor ( 0, 0, 0, 150 ))
    dxDrawText ( text, x, y, x + textWidth, y + 20, tocolor ( 255, 255, 255, 255 ), 1, fontSize, fontName, "center", "center" )
end

function letMeIn()
    if gateCols[currentCol] then
        triggerServerEvent("onGateOpen", localPlayer, gateCols[currentCol])
        hatarDialogHide()
    end
end
addEventHandler ( "onClientGUIClick", button, letMeIn )

function exitVehicleInBorder (theVehicle, seat)
    if seat == 0 and show then
        hatarDialogHide()
    end
end
addEventHandler ("onClientPlayerVehicleExit", root, exitVehicleInBorder)

function enterVehicleInBorder (theVehicle, seat)
    if (seat == 0) and gateCols[currentCol] and isElementWithinColShape(theVehicle, currentCol) then
        hatarDialogShow()
    end
end
addEventHandler ("onClientPlayerVehicleEnter", root, enterVehicleInBorder)
