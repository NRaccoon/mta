local screenWidth, screenHeight = guiGetScreenSize ( ) 
local fontName = "pricedown"
local fontSize = 1
local x = screenWidth / 2 
local y = screenHeight / 2 
local cubeDb = nil
local rowCount = nil
local remainingCube = nil

addCommandHandler("addcube", function( commandName, db )
    if(type(tonumber(db))=="number") then
        cubeDb = math.min(math.abs(db)-1,116)
        rowCount = math.min(math.floor(math.abs(db)/14),8)
    else
        outputChatBox("He te buzi vagy? Számot adjál már meg!")
    end
end)

function createText ( )
    if cubeDb and rowCount then
        dxDrawRectangle ( x, y, 550, 10+ (rowCount+1) * 42, tocolor ( 0, 0, 0, 150 ))
        remainingCube = cubeDb
        for i=0, rowCount do
            for j=0, math.min(remainingCube, 12) do
                if (i==7 and (j==2 or j==3) or (i==4 and j==6)) then
                    dxDrawRectangle ( x + 10 + (j*42), y + 10 + (i*42), 32, 32, tocolor ( 255, 255, 255, 255 ))
                elseif(cubeDb==116 and (((j==1 or j==5 or j==7) and (i~=8 and i~= 0)) or ((i == 1 or i== 4 or i==7) and (j~=0 and j~=4 and j~=8 and j~=12) or (j==9 and (i==2 or i== 3)) or (j==11 and (i==5 or i==6))))) then
                    dxDrawRectangle ( x + 10 + (j*42), y + 10 + (i*42), 32, 32, tocolor ( 255, 0, 0, 255 ))
                else
                    dxDrawRectangle ( x + 10 + (j*42), y + 10 + (i*42), 32, 32, tocolor ( 255, 255, 255, 255 ))
                end
            end
            remainingCube= remainingCube-13
        end
    end
end

addEventHandler ( "onClientRender", root, createText )