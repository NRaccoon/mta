local kocsi = createVehicle(498, Vector3(unpack(carPos)))
local controls = {'fire', 'aim_weapon', 'next_weapon', 'previous_weapon', 'jump', 'sprint', 'crouch', 'enter_exit'}
local box = nil
local boxDarab = 0
local currentBoxPos = Vector3(boxPos)

function createBox()
    box = createObject(1221, currentBoxPos)
    setObjectScale (box, 0.5)
    setElementFrozen (box, true)
end
addEventHandler ("onResourceStart", root, createBox)

function boxFelvesz()
    setElementFrozen (box, false)
    exports.bone_attach:attachElementToBone(box, client, 11, -0.2, 0.2, 0.13, 270, 20, 35);
    triggerClientEvent("onBoxHand", root)
    setPedAnimation(client, "carry", "crry_prtial", 0, false, true, true, true)
    for k, v in pairs(controls) do
        toggleControl (client, v, false) 
    end
end
addEvent("onFelvesz", true)
addEventHandler("onFelvesz", root, boxFelvesz)

function boxLerak(kocsibaE)
    if kocsibaE then 
        if boxDarab < 5 then
            setPedAnimation(client, "carry", "putdwn", -1, false, false, false, false)
            destroyElement(box)
            triggerClientEvent("onBoxHand", root)
            for k, v in pairs(controls) do
                toggleControl (client, v, true)
            end
            boxDarab = boxDarab + 1
            createBox()
        end
        triggerClientEvent("onDarabKiir", client, boxDarab)
    else
        setPedAnimation(client, "carry", "putdwn", -1, false, false, false, false)
        exports.bone_attach:detachElementFromBone(box);
        local pos = client.matrix:transformPosition(Vector3(0, 1, -0.7))
        setElementPosition(box, pos.x, pos.y, pos.z)
        setElementRotation(box, 0, 0, 5)
        setElementFrozen (box, true)
        triggerClientEvent("onBoxHand", root, pos.x, pos.y, pos.z)
        for k, v in pairs(controls) do
            toggleControl (client, v, true)
        end
    end
end
addEvent("onLerak", true)
addEventHandler("onLerak", root, boxLerak)
