local screenWidth, screenHeight = guiGetScreenSize ( )
local fontName = dxCreateFont("Pacifico.ttf", 55)
local timer = nil
local text = nil
local boxInHand = false
local boxCurrentPos = Vector3(boxPos)

function boxMozgat(button, press)
    if (press) and button == 'e' then
        local px, py, pz = getElementPosition(localPlayer)
        local boxDis = getDistanceBetweenPoints3D(px,py,pz,boxCurrentPos)
        local carDis = getDistanceBetweenPoints3D(px,py,pz,Vector3(unpack(carPos)))
        if boxDis < 1.5 and not boxInHand then
            triggerServerEvent("onFelvesz", localPlayer)
        elseif carDis < 4 and boxInHand then
            triggerServerEvent("onLerak", localPlayer, true)
        elseif boxInHand then
            triggerServerEvent("onLerak", localPlayer, false)
        end
    end   
end
addEventHandler("onClientKey", root, boxMozgat)

function darabKiir(boxDarab)
    if boxDarab < 5 then
        text = boxDarab.."/5"
    else 
        text = "Megtelt az autó, nem fér rá több!"
    end
    timer = setTimer(function() end, 2000, 1)
end
addEvent("onDarabKiir", true)
addEventHandler("onDarabKiir", root, darabKiir)

function createText ()
    if timer and isTimer(timer) then
        local textWidth = dxGetTextWidth (text)
        dxDrawText ( text, screenWidth/2 - textWidth/2, 20, textWidth, 20, tocolor ( 255, 255, 255, 255 ), 1, 1, fontName)
    end
end
addEventHandler ( "onClientRender", root, createText )


function boxInHandE(bx, by, bz)
    boxInHand = not boxInHand
    if bx and by and bz then
        boxCurrentPos = Vector3(bx, by, bz)
    else
        boxCurrentPos = Vector3(boxPos)
    end
end
addEvent("onBoxHand", true)
addEventHandler("onBoxHand", root, boxInHandE)