local text = "Na faszom, kezdő szöveg!"
local screenWidth, screenHeight = guiGetScreenSize ( ) 
local ido = 15
local timer = nil

function playerPressedKey(button, press)
    if (press) and button == 'e' and isElementWithinMarker(localPlayer, CJHouseMark) then -- Only output when they press it down
        triggerServerEvent('joska', localPlayer)
    end
end
addEventHandler("onClientKey", root, playerPressedKey)

function createText ( )
    if timer and isTimer(timer) then
        if not text then return end
        local fontName = "pricedown"
        local fontSize = 1
        
        local textWidth = dxGetTextWidth ( text, fontSize, fontName )
        local textHeight = dxGetFontHeight ( fontSize, fontName )
        
        local x = screenWidth / 2 - textWidth / 2
        local y = screenHeight / 2 - textHeight / 2
        local dinamicWidth = textWidth * (getTimerDetails(timer) / 1000 / ido)

        dxDrawRectangle ( x - 10, y - 10, textWidth + 20, textHeight + 20, tocolor ( 0, 0, 0, 150 ))
        dxDrawText ( text, x, y, x + textWidth, y + textHeight, tocolor ( 255, 255, 255, 255 ), 1, fontSize, fontName, "center", "center" )
        dxDrawRectangle ( x, y +100, textWidth, 25, tocolor ( 0, 0, 0, 150 ))
        dxDrawRectangle ( x, y +100,  dinamicWidth, 25, tocolor (255, 255, 255, 255 ))
    end
end

addEventHandler ( "onClientRender", root, createText )

-- addEventHandler( "onClientResourceStart", root, function()
--     timer = setTimer(function() end, ido*1000, 1);
-- end);


addEvent ( "onGreeting", true )

addEventHandler ( "onGreeting", root, function( time )
    outputDebugString("Most lefut az ongreeting")
    ido = time
	timer = setTimer(function() end, time*1000, 1);
end )