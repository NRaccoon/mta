local text = nil
local screenWidth, screenHeight = guiGetScreenSize ( ) 
local vehiclearr = {}

addEvent ( "onValamiEvent", true )

addEventHandler ( "onValamiEvent", root, function( ptext )
	outputDebugString(getPlayerName(source))

	text = ptext
end )

addCommandHandler("cc", function( commandName, ... )
    local arg = {...}
	text = table.concat( arg, " " )
	triggerServerEvent ( "onSpecialEvent", resourceRoot, text )
end)

function createText ( )
	if not text then return end
	local fontName = "pricedown"
	local fontSize = 1
	
	local textWidth = dxGetTextWidth ( text, fontSize, fontName )
	local textHeight = dxGetFontHeight ( fontSize, fontName )
	
    local x = screenWidth / 2 - textWidth / 2
	local y = screenHeight / 2 - textHeight / 2
	

    dxDrawRectangle ( x - 10, y - 10, textWidth + 20, textHeight + 20, tocolor ( 0, 0, 0, 150 ))
    dxDrawText ( text, x, y, x + textWidth, y + textHeight, tocolor ( 255, 255, 255, 255 ), 1, fontSize, fontName, "center", "center" )
end

addEventHandler ( "onClientRender", root, createText )

function valami ()
	-- if vehiclearr then
		local x,y,z = getElementPosition( localPlayer );
		for k, v in pairs(vehiclearr) do 
			if isElement(k) then
				local vx,vy,vz = getElementPosition( k );
				local distance = getDistanceBetweenPoints3D( x, y, z, vx, vy, vz );
				if distance < 25 then
				outputDebugString("Ez a kocsi: "..getVehicleName(k).." ennyire van:")
					local lx, ly = getScreenFromWorldPosition (vx, vy, vz)
					if lx and ly then
					  dxDrawRectangle ( lx, ly, 40, 40, tocolor ( 0, 0, 0, 150 ))
					  dxDrawText ( "Kocsi név: "..getVehicleName(k), lx, ly, lx + 40, ly + 40, tocolor ( 255, 255, 255, 255 ), 1, 1, "default", "center", "center" )
					end
				end
			end
		end
end
addEventHandler ( "onClientRender", root, valami )

addEventHandler( "onClientElementStreamIn", root,
function ( )
	if getElementType( source ) == "vehicle" then
		--local myPosTab = { getElementPosition( localPlayer ) };
		-- local markerPosTab = { getElementPosition( source ) };
		-- local distance = getDistanceBetweenPoints3D( unpack( myPosTab ), unpack( markerPosTab ) );
		-- table.insert(vehiclearr, source)
		vehiclearr[source] = 1
		outputChatBox( "A vehicle has just streamed in. Distance to the vehicle: " .. getVehicleName ( source ).."." );
	end
end
);

addEventHandler( "onClientElementStreamOut", root,
function ( )
	if getElementType( source ) == "vehicle" and vehiclearr[source] then
		--local myPosTab = { getElementPosition( localPlayer ) };
		-- local markerPosTab = { getElementPosition( source ) };
		-- local distance = getDistanceBetweenPoints3D( unpack( myPosTab ), unpack( markerPosTab ) );
		vehiclearr[source]=nil
		outputChatBox( "A vehicle has just streamed out. Distance to the vehicle: " .. getVehicleName ( source ).."." );
	end
end
);