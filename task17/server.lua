local mysql = exports.nx_mysql
local objects = {}

addCommandHandler("lerak", function(playerSource, commandName)
    local rx, ry, rz = getElementRotation(playerSource)
	local pos = playerSource.matrix:transformPosition(Vector3(2, 0, 0))
	local model = math.random(1598, 1609)
	local obj = createObject(model, pos.x, pos.y, pos.z, rx, ry, rz)
	local dim = getElementDimension(playerSource)
	local inter = getElementInterior(playerSource)
	setElementDimension(obj, dim)
    setElementInterior(obj, inter)
	dbQuery(function(qh, model, x, y, z, rotx, roty, rotz, interior, dimension, obj)
        local row, nums, id = dbPoll(qh, 0, true);
        if row and row[1] then
            objects[(id or 0)+1] = obj
        else
            outputDebugString("Hiba! SQL 127");
        end
    end, {model, x, y, z, rotx, roty, rotz, interior, dimension}, mysql:getConnection(), "INSERT INTO `objektek` SET `model` = ?, `x` = ?, `y` = ?, `z` = ?, `rotx` = ?, `roty` = ?, `rotz` = ?, `interior` = ?, `dimension` = ?",model, pos.x, pos.y, pos.z, rx, ry, rz, inter, dim);
end)

dbQuery(function(query)
	local query, query_lines = dbPoll(query, 0);
	if query_lines > 0 then
		outputDebugString("Betölt " .. query_lines .. " darabot!")
		for _, row in ipairs(query) do
			local obj =  createObject(row.model, row.x, row.y, row.z, row.rotx, row.roty, row.rotz)
			setElementDimension(obj, row.dimension)
    		setElementInterior(obj, row.interior)
			objects[row.id] = obj
		end
	else
		outputDebugString("Nincs semmi!")
	end
end, mysql:getConnection(), "SELECT * FROM `objektek`");

addCommandHandler("delete", function(playerSource, commandName, dbid)
	dbQuery(function(query, dbid)
        local _, _, id = dbPoll(query, 0);
		outputDebugString(inspect(id))
        if tonumber(id) or 0 > 0 then
			destroyElement(objects[tonumber(dbid)])
        else
            outputDebugString("[OBJEKTEK] Error with delete object: " .. (id or ""));
        end
    end, {dbid}, mysql:getConnection(), "DELETE FROM `objektek` WHERE `id` = ?", dbid);
end)

addCommandHandler("kiir", function(playerSource, commandName)
	local pos = playerSource.matrix:transformPosition(Vector3(0, 0, 0))
	dbQuery(function(query)
		local query, query_lines = dbPoll(query, 0);
		if query_lines > 0 then
			for _, row in ipairs(query) do
				outputDebugString("Adatabázis id: "..row.id..", model id: "..row.model)
			end
		end
	end, mysql:getConnection(), "SELECT id, model FROM `objektek` WHERE abs(`x` - ?) < 20 AND abs(`y` - ?) < 20", pos.x, pos.y)
end)