local fires = {}
local tuzCols = {}
local cols = {}
local fontName = dxCreateFont("Pacifico.ttf", 50)
local show = false
local olt = false
local controls = {'fire', 'aim_weapon', 'next_weapon', 'previous_weapon', 'jump', 'sprint', 'crouch', 'enter_exit'}
local timer = nil

addCommandHandler("kezd", function()
    if not show then
        triggerServerEvent("onTuzoltoKezd", localPlayer)
        show = true
        addEventHandler ( "onClientRender", root, showSomeFlame)
    end
end)

addCommandHandler("befejez", function()
    if show then
        triggerServerEvent("onTuzoltoBefejez", localPlayer)
        removeEventHandler("onClientRender", root, showSomeFlame)
        show = false
    end
end)

function onFire(theElement, matchingDimension)
    if theElement == localPlayer and matchingDimension and tuzCols[source] and not fireProof[getElementModel(theElement)] then
        triggerServerEvent("imOnFire", localPlayer)
    end
    if cols[source] and theElement == localPlayer and matchingDimension then
        fires[cols[source]][2] = true
    end
end
addEventHandler ("onClientColShapeHit", root, onFire)

function onFarAway(theElement, matchingDimension)
    if cols[source] and theElement == localPlayer and matchingDimension then
        fires[cols[source]][2] = false
    end
end
addEventHandler ("onClientColShapeLeave", root, onFarAway)

function olt(button, press)
    local x, y, z = getElementPosition(localPlayer)
    local rx, ry, rz = getElementRotation(localPlayer)
    if press and button == 'x' then
        for k,v in pairs(fires) do
            if v[2] then
                setPedAnimation(localPlayer, "paulnmac", "piss_out", -1, true, true, true, true) 
                for i, j in pairs(controls) do
                    toggleControl(j, false)
                end
                local px, py, pz = getElementPosition(k)
                tr = findRotation(x, y, px, py+1)
                if tr -30 <= rz and rz <= tr+30 then
                    timer = setTimer(function()
                        outputChatBox(inspect(v[1]))
                        local hp = v[1]-1
                        if hp == 0 then
                            triggerServerEvent("onTuzEloltva", localPlayer)
                            fires[k] = {}
                            destroyElement(cols[v[2]])
                            destroyElement(cols[v[3]])
                            destroyElement(k)
                        end
                        v[1] = hp
                    end, 100, 100)
                end
            end
        end
    end
    if not press and button == 'x' and olt then
        if timer and isTimer(timer) then
            killTimer(timer)
        end
        setPedAnimation(localPlayer)
        for k, v in pairs(controls) do
            toggleControl( v, true)
        end
    end
end
addEventHandler("onClientKey", root, olt)


function showSomeFlame()
    if show then
        outputChatBox(inspect(fires))
        for k, v in pairs(fires) do
            local ox, oy, oz = getElementPosition(k)
            local px, py, pz = getElementPosition(localPlayer)
            local dis = getDistanceBetweenPoints3D(ox, oy, oz, px, py, pz)
            if dis < 15 then
                local lx, ly = getScreenFromWorldPosition(ox, oy+1, oz+2)
                if lx and ly and isLineOfSightClear(ox, oy, oz, px, py, pz, true, true, false, false, false, false, false, localPlayer) then
                    local fontSize = getEasingValue(math.max(math.min((15 - dis) / 15, 1), 0.2), "OutQuad")
                    dxDrawText(v[1], lx, ly, lx + 20 , ly + 20, tocolor ( 255, 255, 255, 255 * fontSize ), fontSize, fontSize, fontName,"center", "center", false, false, false, false, true)
                end
            end
        end
    end
end

function findRotation( x1, y1, x2, y2 ) 
    local t = -math.deg( math.atan2( x2 - x1, y2 - y1 ) )
    return t < 0 and t + 360 or t
end

addEvent("onTuzekSpawn", true)
addEventHandler("onTuzekSpawn", root, function()
    for k,v in pairs(tuzPos) do
        fire = createEffect("fire_large", Vector3(v), 0, 0, 0, 0, true)
        -- fires[fire] = k
        col = createColSphere(v[1], v[2]+1, v[3], 3)
        tuzCol = createColSphere(v[1], v[2]+1, v[3], 1.5)
        fires[fire] = {100, false, col, tuzCol}
        cols[col] = fire
        tuzCols[tuzCol] = true
        -- if not getElementData(fire, k) then
        --     setElementData(fire, k, 100)
        -- end
    end
end)

addEvent("onTuzoltoJobEnd", true)
addEventHandler("onTuzoltoJobEnd", root, function()
    show = false
    for k,v in pairs(fires) do
        destroyElement(k)
    end
    fires = nil
end)