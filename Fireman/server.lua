local tuzek = {}

addEvent("onTuzoltoKezd", true)
addEventHandler("onTuzoltoKezd", root, function()
    porolto = createObject(366, getElementPosition(source))
    exports.bone_attach:attachElementToBone(porolto, source, 12, 0.1, 0, 0, 180, 50, 200);
    triggerClientEvent("onTuzekSpawn", source, tuzek)
end)

addEvent("onTuzoltoBefejez", true)
addEventHandler("onTuzoltoBefejez", root, function()
    triggerClientEvent("onTuzoltoJobEnd", source)
end)

addEvent("imOnFire", true)
addEventHandler("imOnFire", root, function()
    setPedOnFire(source, true)
end)

addEvent("onTuzEloltva", true)
addEventHandler("onTuzEloltva", root, function(tuz)
    givePlayerMoney(source, 1000)
end)
