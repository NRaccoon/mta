local conns = {}

function connect(db)
	if isElement(conns[db]) then
		destroyElement(conns[db]);
	end
	local conn = dbConnect("mysql","dbname=".. db ..";host="..get("mysql_host")..";port="..(get("mysql_port") or 3306), get("mysql_username"), get("mysql_password"), "autoreconnect=1");
	if not conn then
		setTimer(connect, 1000, 1, db);
		outputDebugString("[MYSQL] Csatlakozás sikertelen!");
		return false;
	else
        conns[db] = conn;
		outputDebugString("[MYSQL] Csatlakozás sikeres! ("..db..")");
		return conns[db];
	end
end

addEventHandler("onResourceStart", resourceRoot, function()
	local state = connect(get("mysql_database"));
	if not state then
		cancelEvent(true);
	end
end);

addEventHandler("onResourceStop", resourceRoot, function()
	if isElement(conn) then
		destroyElement(conn);
	end
end);

function getConnection(db)
    if not db then 
    	db = get("mysql_database");
    end
	return conns[db] or nil;
end

function exec(sql, ...)
	dbExec(getConnection(), sql, unpack({...}));
end