local screenWidth, screenHeight = guiGetScreenSize ( )
local fontName = dxCreateFont("hatar.ttf", 20)
local fontSize = 1
local text = "A határon való átkeléshez fizess 500 dollárt!"
local textWidth = dxGetTextWidth ( text, fontSize, fontName )
local x = screenWidth / 2 - textWidth / 2
local y = screenHeight / 2
local show = false
local button = guiCreateButton(0.48, 0.55, 0.05, 0.05, "Kifizet", true)
guiSetVisible(button, false)
local currentBorderElement = nil

function playerPressedKey(button, press)
    if (press) and button == 'm' then
        showCursor ( not isCursorShowing ( ) )
    end
    if (press) and button == "enter" and show then
        atkelesFizetve()
        cancelEvent()
    end
end
addEventHandler("onClientKey", root, playerPressedKey)

function borderPaymentDialog ( showHide, colElement )
    currentBorderElement = colElement
    show = showHide
    guiSetVisible(button, show)
    if show then
        addEventHandler ( "onClientRender", root, createText )
    else
        removeEventHandler ( "onClientRender", root, createText )
    end
end
addEvent( "onBorderPayment", true )
addEventHandler( "onBorderPayment", localPlayer, borderPaymentDialog)

function createText ( )
    dxDrawRectangle ( x - 10, y - 10, textWidth + 20, 40, tocolor ( 0, 0, 0, 150 ))
    dxDrawText ( text, x, y, x + textWidth, y + 20, tocolor ( 255, 255, 255, 255 ), 1, fontSize, fontName, "center", "center" )
end

function atkelesFizetve()
    if getPlayerMoney() >= 500 then
        show = false
        guiSetVisible(button, false)
        takePlayerMoney(500)
        triggerServerEvent ( "onGateOpen", resourceRoot, currentBorderElement )
    else 
        show = false
        guiSetVisible(button, false)
        outputChatBox('Határőr kikiált: "Csóró vagy, takarodj innen!"')
    end
end
addEventHandler ( "onClientGUIClick", button, atkelesFizetve )