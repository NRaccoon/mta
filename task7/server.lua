local borders = {
    {988, 1784.80048, 816.47809, 10.80644, 1, 0, 180}, {1684, 1779.86560, 820.13342, 11.24101, 0, 1.5, 90},
    {980, 1793.04048, 816.47809, 10.80644, 1, 0}, {988, 1808.98048, 816.47809, 10.80644, 1, 0},
    {1684, 1813.86560, 812.13342, 11.58101, 0, -0.9, 270}, {980, 1800.34048, 816.47809, 10.80644, 1, 0, 180}
}
local borderColList = {}

function createBorders ()
	for k, v in pairs(borders) do
        if k%3==1 then
            local gate = createObject(unpack(v))
            local x, y, z = getElementPosition(gate)
            local borderCol =  createColCircle(x, y, 6)
            borderColList[borderCol] = gate
        else 
            createObject(unpack(v))
        end
    end
end
addEventHandler ( "onResourceStart", root, createBorders )

function borderColShapeHit( colShapeHit )
    if borderColList[colShapeHit] then
        triggerClientEvent ( source, "onBorderPayment", source, true, colShapeHit)
    end
end
addEventHandler( "onElementColShapeHit", root, borderColShapeHit )

function gateOpener (borderElement)
	outputChatBox (getPlayerName(client) ..", átkelhet a határon.")
    local gate = borderColList[borderElement]
    local x,y,z = gate:getPosition()
    local pos = gate.matrix:transformPosition(Vector3(-5, 0, 0))
    moveObject(gate, 1000, pos.x, pos.y, pos.z)
    setTimer( function()
        moveObject(gate, 1000, x,y,z)
    end, 3000, 1);
    triggerClientEvent ( client, "onBorderPayment", client, false)
end
addEvent( "onGateOpen", true )
addEventHandler( "onGateOpen", resourceRoot, gateOpener )

function borderColShapeHit( colShapeLeft )
    if borderColList[colShapeLeft] then
        triggerClientEvent ( source, "onBorderPayment", source, false)
    end
end
addEventHandler( "onElementColShapeLeave", root, borderColShapeHit )


-- a határokat egy táblából hozza létre a rendszer, előre megadott poziíciókból
-- a határ automatikusan 5 mp múlva záródjon be