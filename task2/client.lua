local boxDarab = 0
local boxes = {}

addCommandHandler("/lerak", function( commandName )
    local x, y, z = getElementPosition(localPlayer)
    local doboz = createObject(917, x, y, z-0.85)
    boxDarab = boxDarab + 1
    boxes[doboz] = boxDarab
    setElementData(doboz, "id", boxDarab)
end)

addCommandHandler("/objectek", function( commandName )
    outputChatBox(boxDarab)
end)

function playerPressedKey(button, press)
    if (press) and button == 'm' then
        showCursor ( not isCursorShowing ( ) )
    end
end
addEventHandler("onClientKey", root, playerPressedKey)

function dobozGone ( button, state, absoluteX, absoluteY, worldX, worldY, worldZ, clickedElement )
    if (state == 'down') then
        if ( getElementType(clickedElement) == "object" and getElementData(clickedElement, "id")) then
            local px,py,pz = getElementPosition( localPlayer );
            local x,y,z = getElementPosition( clickedElement );
            local distance = getDistanceBetweenPoints3D (x, y, z, px, py, pz)
            if(distance < 50) then
                if (getElementData(clickedElement, "id") % 2 ~= 0) then
                    boxes[doboz] = nil
                    boxDarab = boxDarab - 1
                    destroyElement(clickedElement)
                else 
                    outputChatBox("Nem törölhető!") 
                end
            end
        end
    end
end
addEventHandler ( "onClientClick", root, dobozGone)