
function kocsiNamePrint()
	if streamedCars then
		local x,y,z = getElementPosition( localPlayer );
		for k, v in pairs(streamedCars) do 
			if isElement(k) then
				local vx,vy,vz = getElementPosition( k );
				local distance = getDistanceBetweenPoints3D( x, y, z, vx, vy, vz );
				if distance < 50 then
				outputDebugString("Ez a kocsi: "..getVehicleName(k).." ennyire van:")
					local lx, ly = getScreenFromWorldPosition (vx, vy, vz)
					if lx and ly then
					  dxDrawRectangle ( lx, ly, 40, 40, tocolor ( 0, 0, 0, 150 ))
					  dxDrawText ( "Kocsi név: "..getVehicleName(k), lx, ly, lx + 40, ly + 40, tocolor ( 255, 255, 255, 255 ), 1, 1, "default", "center", "center" )
					end
				end
			end
		end
    end
end
addEventHandler ( "onClientRender", root, kocsiNamePrint )

addEventHandler( "onClientElementStreamIn", root,
function ( )
	if getElementType( source ) == "vehicle" then
		streamedCars[source] = 1
		outputChatBox( "A vehicle has just streamed in. Distance to the vehicle: " .. getVehicleName ( source ).."." );
	end
end
);

addEventHandler( "onClientElementStreamOut", root,
function ( )
	if getElementType( source ) == "vehicle" and streamedCars[source] then
		streamedCars[source]=nil
		outputChatBox( "A vehicle has just streamed out. Distance to the vehicle: " .. getVehicleName ( source ).."." );
	end
end
);