local cars = getElementsByType ( "vehicle" )

for k, v in ipairs(cars) do
    setElementData (v, k, getVehicleName(v))
    outputChatBox(getElementData(v, k))
end
