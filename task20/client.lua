local cols = {}
local screenWidth, screenHeight = guiGetScreenSize ( )
local fontName = dxCreateFont("furas.ttf", 20)
local fontSize = 1
local y = screenHeight / 2
local show = false
local index = 0
local currentCol = nil
local ercekFound = ""
local erceimDB = nil
local vanErcem = false

addCommandHandler("/lerak", function( commandName, scale )
    local koPos = Matrix(Vector3(getElementPosition(localPlayer)), Vector3(getElementRotation(localPlayer)))
    local pos = koPos:transformPosition(Vector3(0, 10, 0))
    if scale then
        scale = math.min(math.max(scale+2.2, 3.2), 8.2)
    else
        scale = 6.2
    end
    local col = createColSphere(pos.x, pos.y, pos.z, scale)
    index = index + 1
    cols[col] = index
    triggerServerEvent("onKoLerak", localPlayer, pos.x, pos.y, pos.z, 10.2-scale, index)
end)

function furasShow (theElement, matchingDimension)
    if getElementType(theElement) == "player" and cols[source] and matchingDimension then
        show = true
        currentCol = source
        if show then
            addEventHandler("onClientRender", root, createText)
        end
    end
end
addEventHandler ("onClientColShapeHit", root, furasShow)

function furasHide (theElement, matchingDimension)
    if getElementType(theElement) == "player" and cols[source] and matchingDimension then
        currentCol = nil
        if show then
            removeEventHandler("onClientRender", root, createText)
        end
        show = false
    end
end
addEventHandler ("onClientColShapeLeave", root, furasHide)

function createText ()
    local text = "Most fúrhatsz (space)"
    local textWidth = dxGetTextWidth ( text, fontSize, fontName )
    local x = screenWidth / 2 - textWidth / 2
    dxDrawRectangle ( x - 10, y - 10, textWidth + 20, 40, tocolor ( 0, 0, 0, 150 ))
    dxDrawText ( text, x, y, x + textWidth, y + 20, tocolor ( 255, 255, 255, 255 ), 1, fontSize, fontName, "center", "center" )
end

function furasPressed(button, press)
    if (press) and button == "space" and show and cols[currentCol] then
        if show then
            removeEventHandler("onClientRender", root, createText)
        end
        show = false
        local x, y, z = getElementPosition(currentCol)
        triggerServerEvent("onFuras", localPlayer, cols[currentCol], x, y, z)
        cols[currentCol] = nil
        destroyElement(currentCol)
        cancelEvent()
    end
end
addEventHandler("onClientKey", root, furasPressed)

addEvent("onErcFound", true)
addEventHandler("onErcFound", root, function(talaltErcek)
    if not erceimDB then erceimDB = {szen = 0, vas = 0, arany = 0, lazulit = 0, zafir = 0} end
    ercekFound = ""
    for k,v in pairs(talaltErcek) do
        ercekFound = ercekFound..k..", "
        erceimDB[k] = v
    end
    ercekFound = string.sub(ercekFound, 1, -3)
    addEventHandler("onClientRender", root, ercShow)
    timer = setTimer(function()
         removeEventHandler("onClientRender", root, ercShow)
    end, 5000, 1)
end)

function ercShow()
    local text = "A következő ércet/érceket találtad: "..ercekFound
    local textWidth = dxGetTextWidth(text, fontSize, fontName)
    local x = screenWidth / 2 - textWidth / 2
    local y = screenHeight / 3
    dxDrawRectangle(x - 10, y - 10, textWidth + 20, 40, tocolor ( 0, 0, 0, 150 ))
    dxDrawText(text, x, y, x + textWidth, y + 20, tocolor ( 255, 255, 255, 255 ), 1, fontSize, fontName, "center", "center")
end

addEvent("onSpawnKo", true)
addEventHandler("onSpawnKo", root, function(x, y, z)
    local scale = math.random(3, 8)+0.2
    local col = createColSphere(x, y, z, scale)
    index = index + 1
    cols[col] = index
    triggerServerEvent("onKoLerak", localPlayer, x, y, z, 10.2-scale, index)
end)

addEvent("onBarShow", true)
addEventHandler ("onBarShow", root, function()
    addEventHandler("onClientRender", root, createBar)
    timer = setTimer(function()
        removeEventHandler("onClientRender", root, createBar)
    end, 5010, 1)
end)

function createBar()
    local x = screenWidth / 2 -  150
    local dinamicWidth = 300 * (getTimerDetails(timer) / 5010)
    dxDrawRectangle(x, y +100, 300, 25, tocolor ( 0, 0, 0, 150 ))
    dxDrawRectangle(x, y +100,  dinamicWidth, 25, tocolor (255, 255, 255, 255 ))
end

function inventoryErcShow()
    if erceimDB then
        local text = ""
        for k,v in pairs(erceimDB) do
            text = text ..k..": "..v.." db\n"
        end
        local textWidth = dxGetTextWidth(text, fontSize, fontName)
        local x = screenWidth - textWidth - 20
        dxDrawRectangle(x - 10, y - 80, textWidth + 20, 10 + 180, tocolor ( 0, 0, 0, 150 ))
        dxDrawText(text, x, y, x + textWidth, y + 20, tocolor ( 255, 255, 255, 255 ), 1, fontSize, fontName, "center", "center")
    end
end
addEventHandler("onClientRender", root, inventoryErcShow)

addEventHandler("onClientResourceStart", root, function()
    triggerServerEvent("onErceim", localPlayer)
end);

addEvent("onErcekBeallit", true)
addEventHandler("onErcekBeallit", root, function(erceim)
    erceimDB = erceim
end)

addCommandHandler("/eladas", function( commandName, scale )
    triggerServerEvent("onErcekElad", localPlayer)
end)