local kovek = {}
local mysql = exports.nx_mysql
local ercekDB = {}

addEvent("onKoLerak", true)
addEventHandler("onKoLerak", root, function(x, y, z, scale, index)
    local ko = createObject(897, x, y, z-scale)
    kovek[index] = ko
end)


addEvent("onFuras", true)
addEventHandler("onFuras", root, function(index, x, y, z)
    if getElementType(source)=="player" and kovek[index] then
        local player = source
        local player_nev = getPlayerName(player)
        setPedAnimation(player, "medic", "cpr", 5000, false, false, false, false) 
        toggleAllControls(player, false)
        triggerClientEvent("onBarShow", root) 
        setTimer(function()
            toggleAllControls(player, true)
            destroyElement(kovek[index])
            kovek[index] = nil
            local talaltErcek = {}
            for k,v in pairs(ercek) do
                local currentDarab = 0
                if ercekDB[player_nev] then
                    currentDarab = (ercekDB[player_nev][k])
                end
                if math.random(1,100) <= v then
                    currentDarab =  currentDarab + 1
                    talaltErcek[k] = currentDarab
                    if ercekDB[player_nev] then
                        dbExec(mysql:getConnection(), "UPDATE `ercek` SET ?? = ? WHERE `player_nev` = ?",  k.."_db", currentDarab, player_nev);
                    else
                        dbExec(mysql:getConnection(), "INSERT INTO `ercek` SET `player_nev` = ?, ?? = ?", player_nev, k.."_db", currentDarab);
                        ercekDB[player_nev] = {szen = 0, vas = 0, arany = 0, lazulit = 0, zafir = 0}
                    end
                    ercekDB[player_nev][k] = currentDarab
                end
            end
            if not next(talaltErcek) then
                playSoundFrontEnd(player, 10)
            else
                triggerClientEvent("onErcFound", root, talaltErcek)
                playSoundFrontEnd(player, 101)
            end
            setTimer(function()
                triggerClientEvent("onSpawnKo", root, x, y, z)
            end, 5000, 1)
        end, 5000, 1)
    end
end)

addEvent("onErceim", true)
addEventHandler("onErceim", root, function()
    if ercekDB[getPlayerName(source)] then
        triggerClientEvent("onErcekBeallit", root, ercekDB[getPlayerName(source)])
    end
end)

function loadErcek()
    dbQuery(function(query)
        local query, query_lines = dbPoll(query, 0);
        if query_lines > 0 then
            for _, row in ipairs(query) do
                ercekDB[row.player_nev] = {szen = row.szen_db, vas = row.vas_db, arany = row.arany_db, lazulit = row.lazulit_db, zafir = row.zafir_db}
            end 
        end
    end, mysql:getConnection(), "SELECT * FROM `ercek`");
end
addEventHandler("onResourceStart", root, loadErcek)

addEvent("onErcekElad", true)
addEventHandler("onErcekElad", root, function()
    local player = getPlayerName(source)
    if ercekDB[player] then
        local osszeg = getPlayerMoney(source)
        for k,v in pairs(ercekDB[player]) do
            osszeg = osszeg + (arak[k] * v)
        end
        ercekDB[player] = nil
        dbExec(mysql:getConnection(), "DELETE FROM `ercek` WHERE `player_nev` = ?", player);
        triggerClientEvent("onErcekBeallit", root, {szen = 0, vas = 0, arany = 0, lazulit = 0, zafir = 0})
        setPlayerMoney(source, osszeg)
    else
        outputChatBox("Nincs érced amit eladhatnál")
    end
end)