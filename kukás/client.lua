local marker = nil
local kukasautom = nil
local kukak = {}
local fellepok = {}
local bedobok = {}
local szemetInHand = nil
local kocsi = false
local felallhat = false
local fentVan = false
local screenWidth, screenHeight = guiGetScreenSize ( )
local fontName = dxCreateFont("kukas.ttf", 40)
local text = "Nyomd meg az E-t a kukásautóra való felszálláshoz!"
local textWidth = dxGetTextWidth ( text, 0.5, fontName )
local x = screenWidth / 2 - textWidth / 2
local y = screenHeight / 2
local currentButton = ""
local lFellepo = nil
local rFellepo = nil

addCommandHandler("kezd", function()
    if not kukasautom then
        triggerServerEvent("onAutoSpawn", localPlayer)
        marker = createMarker(2481.33398, -1698.46777, 12.52598, "cylinder")
    else
        outputChatBox("Már elkezdted ezt a munkát!")
    end
end)

addCommandHandler("teszt", function()
    triggerServerEvent("onTeszt", localPlayer)
end)

function szallitasKifizet(hitElement,matchingDimension)
    local car = getPedOccupiedVehicle(localPlayer)
	if hitElement == localPlayer and car == kukasautom and matchingDimension then
        local db = getElementData(car, 'capacity')
        if db > 0 then
            triggerServerEvent("onSzallitasKifizet", localPlayer, db)
            outputChatBox(db.." zsák szemét leadva, szép munka!")
            setElementData(car, 'capacity', 0)
        end
	end 
end 
addEventHandler("onClientMarkerHit", resourceRoot, szallitasKifizet)

function szemetFelvesz(button, state, _, _, _, _, _, clickedElement)
    if button == 'left' and state == 'down' and not szemetInHand and clickedElement then
        local db = getElementData(clickedElement, "db") or 0
        if db > 0 then
            local px,py,pz = getElementPosition( localPlayer );
            local x,y,z = getElementPosition(clickedElement);
            local distance = getDistanceBetweenPoints3D (x, y, z, px, py, pz)
            if(distance < 2) then
                triggerServerEvent("onSzemetFelvesz", localPlayer, clickedElement, db)
                szemetInHand = clickedElement
            end
        end
    end
    if button == 'left' and state == 'down' and clickedElement and getElementType(clickedElement) == "vehicle" and clickedElement == kocsi and szemetInHand then
        local rx, ry, rz = getElementRotation(clickedElement)
        triggerServerEvent("onSzemetBedob", localPlayer, szemetInHand, rz)
        local cap = getElementData(clickedElement, "capacity")+1
        setElementData(clickedElement, "capacity", cap)
        outputChatBox("A kukásautó telítettsége "..cap.."/100")
        szemetInHand = nil
    end
    if button == 'left' and state == 'up' and currentButton ~= "" then
        if currentButton == "bal" then
            triggerServerEvent("onFellep", localPlayer, lFellepo, kukasautom)
        else
            triggerServerEvent("onFellep", localPlayer, rFellepo, kukasautom)
        end
        removeEventHandler("onClientRender", root, showText)
        felallhat = false
        fentVan = true
    end
end
addEventHandler("onClientClick", root, szemetFelvesz)


function playerPressedKey(button, press)
    if (press) and button == 'm' then
        showCursor(not isCursorShowing())
    end
    if (press) and button == 'e' then
        if felallhat and not fentVan then
            local car = fellepok[felallhat]
            triggerServerEvent("onFellep", localPlayer, felallhat, car)
            removeEventHandler("onClientRender", root, showText)
            felallhat = false
            fentVan = true
        elseif fentVan then
            triggerServerEvent("onLelep", localPlayer)
            fentVan = false
        end
    end
end
addEventHandler("onClientKey", root, playerPressedKey)

function findRotation( x1, y1, x2, y2 ) 
    local t = -math.deg( math.atan2( x2 - x1, y2 - y1 ) )
    return t < 0 and t + 360 or t
end

function onClientColShapeHit( theElement, matchingDimension )
    if theElement == localPlayer and fellepok[source] then
        addEventHandler("onClientRender", root, showText)
        felallhat = source
    end
    if theElement == localPlayer and bedobok[source] then
        kocsi = bedobok[source]
    end
end
addEventHandler("onClientColShapeHit", root, onClientColShapeHit)

function onClientColShapeHit( theElement, matchingDimension )
    if theElement == localPlayer and fellepok[source] then
        removeEventHandler("onClientRender", root, showText)
        felallhat = false
    end
    if theElement == localPlayer and bedobok[source] then
        kocsi = false
    end
end
addEventHandler("onClientColShapeLeave", root, onClientColShapeHit)

function showText()
    dxDrawRectangle ( x - 10, y - 10, textWidth + 20, 40, tocolor ( 0, 0, 0, 150 ))
    dxDrawText ( text, x, y, x + textWidth, y + 20, tocolor ( 255, 255, 255, 255 ), 0.5, 0.5, fontName, "center", "center" )
end

addEvent("onColsGet", true)
addEventHandler("onColsGet", root, function(rUp, lUp, back, car, szemetesek)
    fellepok[rUp] = car
    fellepok[lUp] = car
    bedobok[back] = car
    kukasautom = car
    kukak = szemetesek
    lFellepo = lUp
    rFellepo = rUp
end)


addEventHandler ( "onClientRender", root, function()
    for k, v in pairs(kukak) do
        local ox, oy, oz = getElementPosition(k)
        local px, py, pz = getElementPosition(localPlayer)
        if isElementOnScreen(k) then
            local dis = getDistanceBetweenPoints3D(ox, oy, oz, px, py, pz)
            if dis < 5 then
                local lx, ly = getScreenFromWorldPosition(ox, oy, oz)
                if lx and ly and isLineOfSightClear(ox, oy, oz, px, py, pz, true, true, false, false, false, false, false, localPlayer) then
                    local fontSize = getEasingValue(math.max(math.min((5 - dis) / 15, 1), 0.2), "OutQuad")

                    dxDrawText ( "Szemetes zsákok száma: ".. getElementData(k, "db"), lx, ly, lx + 20 , ly + 20, tocolor ( 255, 255, 255, 255), fontSize, fontSize, fontName,"center", "center", false, false, false, false, true)
                end
            end
        end
    end
    if kukasautom and not fentVan and felallhat then
        local lx, ly = getScreenFromWorldPosition(getElementPosition(lFellepo))
        local rx, ry = getScreenFromWorldPosition(getElementPosition(rFellepo))
        currentButton = ""
        if lx and ly then
            dxDrawRectangle ( lx, ly, 100, 100, tocolor(255, 255, 255, 200))
            if isInBox(lx, ly, 100, 100) then
                currentButton = "bal"
            end 
        end
        if rx and ry then
            dxDrawRectangle ( rx, ry, 100, 100, tocolor(255, 255, 255, 200))
            if isInBox(rx, ry, 100, 100) then
                currentButton = "jobb"
            end 
        end
    end
end)

function isInBox(x, y, w, h)
    if not isCursorShowing() then 
        return false;
    end 
    local cx, cy = getCursorPosition();
    local cx, cy = (cx * screenWidth), (cy * screenHeight); 
    return (cx >= x and cx <= x + w) and (cy >= y and cy <= y + h);
end