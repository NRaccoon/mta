local szemetek = {}
local balFellepok = {}
local szemetesek = {}
local szorzo = 0
local controls = {'fire', 'aim_weapon', 'next_weapon', 'previous_weapon', 'jump', 'sprint', 'crouch', 'enter_exit'}

function kukaSpawner()
	for k,v in pairs(kukak) do
        kuka = createObject(unpack(v))
        setElementData(kuka, "db", 4)
        setElementFrozen(kuka, true)
        szemetesek[kuka] = true
    end
end
addEventHandler ( "onResourceStart", root, kukaSpawner)

addEvent("onSzallitasKifizet", true)
addEventHandler("onSzallitasKifizet", root, function(db)
    setPlayerMoney(source, getPlayerMoney(source)+db*200)
end)

addEvent("onTeszt", true)
addEventHandler("onTeszt", root, function()
    triggerEvent("onAutoSpawn", root)
end)

addEvent("onAutoSpawn", true)
addEventHandler("onAutoSpawn", root, function()
    local kukasauto = createVehicle(408, 2468.58447, -1658.87000, 13.30469, 0, 0, 270)
    local Rfellepo = createColSphere(2454.58447, -1658.87000, 13.30469, 1)
    local Lfellepo = createColSphere(2454.58447, -1658.87000, 13.30469, 1)
    local bedobo = createColSphere(2454.58447, -1658.87000, 13.30469, 1.8)
    attachElements(Rfellepo, kukasauto, 1.8, -3.6, -0.5)
    attachElements(Lfellepo, kukasauto, -1.8, -3.6, -0.5)
    attachElements(bedobo, kukasauto, 0, -5, -0.5)
    balFellepok[Lfellepo] = true
    setElementData(kukasauto, "capacity", 0)
    if source == root then return end
    triggerClientEvent(source, "onColsGet", root, Rfellepo, Lfellepo, bedobo, kukasauto, szemetesek)
end)

addEvent("onSzemetFelvesz", true)
addEventHandler("onSzemetFelvesz", root, function(kuka, db)
    setElementData(kuka, "db", db-1)
    szemet = createObject(1264, getElementPosition(kuka))
    setObjectScale(szemet, 0.5)
    exports.bone_attach:attachElementToBone(szemet, source, 12, -0.05, 0, 0.2, 0, 180)
    szemetek[kuka] = szemet
end)

addEvent("onSzemetBedob", true)
addEventHandler("onSzemetBedob", root, function(kuka, rz)
    setElementRotation(source, 0, 0, rz)
    setPedAnimation(source, "grenade", "weapon_throwu", -1, false, false, false, false)
    setTimer ( function()
		destroyElement(szemetek[kuka])
        szemetek[kuka] = nil
	end, 700, 1 )
    setPlayerMoney(source, getPlayerMoney(source)+200)
end)

addEvent("onFellep", true)
addEventHandler("onFellep", root, function(fellepo, auto)
    if balFellepok[fellepo] then szorzo = -1 else szorzo = 1 end
    attachElements(source, auto, tonumber(szorzo)*1.3, -3.4, -0.12)
    local rx, ry, rz = getElementRotation(auto)
    setElementRotation(source, rx, ry, rz+(tonumber(szorzo)*90))
    setCameraTarget(source, auto)
    for k, v in pairs(controls) do
        toggleControl(source, v, false)
    end
end)

addEvent("onLelep", true)
addEventHandler("onLelep", root, function()
    detachElements(source)
    setCameraTarget(source, source)
    pos = source.matrix:transformPosition(Vector3(0, -1, -1))
    setElementPosition(source, pos.x, pos.y, pos.z)
    for k, v in pairs(controls) do
        toggleControl(source, v, true)
    end
end)