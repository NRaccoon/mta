local cars = {}

function carSpawner (borderElement)
    if source ~= client then return end
    local rx, ry, rz = getElementRotation(source)
    local pos = source.matrix:transformPosition(Vector3(2, 0, 0))
    local car = createVehicle(562, pos.x, pos.y, pos.z, rx, ry, rz)
    setElementDimension(car, getElementDimension(source))
    setElementInterior(car, getElementInterior(source))

    table.insert(cars, car)
end
addEvent("onKocsiLerak", true)
addEventHandler("onKocsiLerak", root, carSpawner)

function kocsiSpawner(id)
    local car = cars[id] or false
    if car then
        local rx, ry, rz = getElementRotation(source)
        local pos = source.matrix:transformPosition(Vector3(2, 0, 0))
        setElementPosition(car, pos.x, pos.y, pos.z)
        setElementRotation(car, rx, ry, rz)
        setElementDimension(car, getElementDimension(source))
        setElementInterior(car, getElementInterior(source))
        outputChatBox("Kaptál egy kocsit fasz", source, 255, 0, 0)
    else
        outputDebugString("ezt az idt probalod: "..id)
    end
end
addEvent("onKocsiSpawn", true)
addEventHandler("onKocsiSpawn", root, kocsiSpawner)
