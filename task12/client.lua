local screenWidth, screenHeight = guiGetScreenSize ( )
local x, y = 10, 10
local xPadding = 10
local yPadding = 10
local size = 160
local pr = 1

function playerPressedKey(button, press)
    if (press) and button == 'F5' then
        local r = math.random(1,4)
        if pr == r then
           r = 5 - pr 
        end
        pr = r
        if r == 1 then
            x = xPadding
            y = yPadding
        elseif r == 2 then 
            x = screenWidth - size - xPadding
            y = yPadding
        elseif r == 3 and pr ~= 1 then 
            x = screenWidth - size - xPadding
            y = screenHeight - size - yPadding
        else
            x = xPadding
            y = screenHeight - size - yPadding
        end
    end
end
addEventHandler("onClientKey", root, playerPressedKey)

function rajz ( )
    dxDrawRectangle ( x , y , 150, 150, tocolor ( 0, 0, 0, 150 ))
end

addEventHandler ( "onClientRender", root, rajz )
