local felveszMarker = createMarker(2026.78784, 1545.95996, 9.52198, "cylinder", 1.5)
local lerakMarker = createMarker(2001.62244, 1544.98096, 12.08594, "cylinder", 1.5)
local controls = {'fire', 'aim_weapon', 'next_weapon', 'previous_weapon', 'jump', 'sprint', 'crouch', 'enter_exit'}
local box = nil

function boxMozgatas ( hitPlayer, matchingDimension )
    if source == felveszMarker and not isElement(box) then
        box = createObject(1221, 2026.78784, 1545.95996, 9.52198)
        setObjectScale ( box, 0.5)
	    exports.bone_attach:attachElementToBone(box, localPlayer, 11, -0.2, 0.2, 0.13, 270, 20, 35);
        setPedAnimation(localPlayer, "carry", "crry_prtial", 0, false, true, true, true)
        for k, v in pairs(controls) do
            toggleControl ( v, false) 
        end
    elseif source == lerakMarker and box then
        setPedAnimation(localPlayer, "RIOT", "RIOT_ANGRY", -1, false, false, false, false)
        destroyElement(box)
        for k, v in pairs(controls) do
            toggleControl ( v, true) 
        end
    end
end
addEventHandler ( "onClientMarkerHit", root, boxMozgatas )