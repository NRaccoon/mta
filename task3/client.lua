
addCommandHandler("/lerak", function( commandName )
    local x, y, z = getElementPosition(localPlayer)
    local doboz = createObject(1271, x, y, z)
    setElementData(doboz, "id", "valami")
end)

function playerPressedKey(button, press)
    if (press) and button == 'm' then
        showCursor ( not isCursorShowing ( ) )
    end
end
addEventHandler("onClientKey", root, playerPressedKey)

function dobozGone ( button, state, absoluteX, absoluteY, worldX, worldY, worldZ, clickedElement )
    local x,y,z = getElementPosition( clickedElement );
    local px,py,pz = getElementPosition( localPlayer );
    local distance = getDistanceBetweenPoints3D (x, y, z, px, py, pz)
    outputChatBox(distance)
    if(50 < distance and distance < 100) then
        moveObject(clickedElement, 1500, px, py, pz)
    end
end
addEventHandler ( "onClientClick", root, dobozGone )