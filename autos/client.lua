local Pulaski = createPed(266, 2492.60669, -1655.48816, 12.79186, 180.0)
local currentMarkerElement = nil;
local currentMarker = 1;


function addTaskOnClick ( button, state, absoluteX, absoluteY, worldX, worldY, worldZ, clickedElement )
        if ( clickedElement == Pulaski and state == 'down') then
            local px,py,pz = getElementPosition( localPlayer );
            local x,y,z = getElementPosition( Pulaski );
            local distance = getDistanceBetweenPoints3D (x, y, z, px, py, pz)
            if(distance < 5) then
                currentMarker = 1
                currentMarkerElement = createMarker(Vector3(unpack(markers[currentMarker])),"cylinder")
                triggerServerEvent ( "onCarSpawnEvent", root)
            end
        end
end
addEventHandler ( "onClientClick", root, addTaskOnClick )

function safePed()
	cancelEvent()
end
addEventHandler("onClientPedDamage", Pulaski, safePed)
addEventHandler("onClientPedHeliKilled", Pulaski, safePed)
addEventHandler("onClientPedHitByWaterCannon", Pulaski, safePed)

function playerPressedKey(button, press)
    if (press) and button == 'm' then
        showCursor ( not isCursorShowing ( ) )
    end
end
addEventHandler("onClientKey", root, playerPressedKey)


function markerSpawner(hitElement,matchingDimension)
    -- local vehicle = getPedOccupiedVehicle(localPlayer);
    -- if (hitPlayer == vehicle) then
    --     outputDebugString("most a kocsidad élrtél hozá")
    -- end
	if (getElementType(hitElement) == "player") then
        currentMarker = currentMarker + 1

        destroyElement(currentMarkerElement)
        currentMarkerElement = createMarker(Vector3(unpack(markers[currentMarker])),"cylinder")
	end 
end 
addEventHandler("onClientMarkerHit", resourceRoot ,markerSpawner)

function handleVehicleDamage(attacker, weapon, loss, x, y, z, tire)
    if (currentMarkerElement and source == getPedOccupiedVehicle(localPlayer)) then
        destroyElement(currentMarkerElement)
	    currentMarkerElement = nil;
        currentMarker = nil;
    end
end
addEventHandler("onClientVehicleDamage", root, handleVehicleDamage)
