local screenWidth, screenHeight = guiGetScreenSize ( )
local image = dxCreateTexture( "kocsi.png" )
local fontName = dxCreateFont("FFF_Tusj.ttf", 24)
local fontSize = 1
local ido = nil
local timer = nil
local text = nil

function createText ( )
    if timer and isTimer(timer) then
        if not text or not image then return end
        local textWidth = dxGetTextWidth ( text, fontSize, fontName )
        local textHeight = dxGetFontHeight ( fontSize, fontName )
        local x = screenWidth / 2 - textWidth / 2
        local y = screenHeight / 2 - textHeight / 2
        local dinamicWidth = textWidth * (getTimerDetails(timer) / 1000 / ido)
        dxDrawImage( x, y-100, 120, 140, image  )
        dxDrawRectangle ( x - 10, y - 10, textWidth + 20, textHeight + 20, tocolor ( 0, 0, 0, 150 ))
        dxDrawText ( text, x, y, x + textWidth, y + textHeight, tocolor ( 255, 255, 255, 255 ), 1, fontSize, fontName, "center", "center" )
        dxDrawRectangle ( x, y +100, textWidth, 25, tocolor ( 0, 0, 0, 150 ))
        dxDrawRectangle ( x, y +100,  dinamicWidth, 25, tocolor (128, 0, 128, 255 ))
    end
end

addEventHandler ( "onClientRender", root, createText )

addEvent("onKocsiNev", true)
addEventHandler ( "onKocsiNev", root, function(time, kocsiNev)
    ido = time
    text = kocsiNev
	timer = setTimer(function() end, time*1000, 1);
end )