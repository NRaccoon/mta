local cars = {
    {558, 2049.27490, 1530.13782, 10.19016}, {559, 2049.27490, 1540.13782, 10.19016},
    {560, 2049.27490, 1550.13782, 10.19016}, {561, 2049.27490, 1560.13782, 10.19016},
    {562, 2049.27490, 1570.13782, 10.19016}, {562, 2040.27490, 1570.13782, 10.19016}
}
local fiveCars = {}
local usedCars = {}

function spawnCars ()
	for k, v in pairs(cars) do
        if k <= 5 then 
            fiveCars[createVehicle(unpack(v))] = true
        else
            createVehicle(unpack(v))
        end
    end
end
addEventHandler ( "onResourceStart", root, spawnCars )

function autoCheck(thePlayer)
    local playerNev = getPlayerName(thePlayer)
    usedCars[playerNev] = usedCars[playerNev] or {}
    local carNev = getVehicleName(source)
    if not usedCars[playerNev][carNev] and fiveCars[source] then
        triggerClientEvent(root, "onKocsiNev", source, "2", carNev)
    end
    usedCars[playerNev][carNev]=true
end
addEventHandler ( "onVehicleEnter", resourceRoot, autoCheck )


