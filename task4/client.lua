local screenWidth, screenHeight = guiGetScreenSize ( ) 
local fontName = dxCreateFont("Pacifico.ttf", 24)
local myImage = dxCreateTexture( "kaki.png" )

local x = screenWidth / 2
local y = screenHeight / 2

addEventHandler( "onClientRender", root, function()
    if myImage then
        dxDrawImage( 100, 350, 300, 350, myImage  )
        dxDrawRectangle ( x - 10, y - 10, 200, 30, tocolor ( 0, 0, 0, 150 ))
        dxDrawText ( "Az ott balra egy kakiszilva :3", x, y, x + 190, y + 20, tocolor ( 255, 255, 255, 255 ), 1, 1, fontName, "center", "center" )
    end
end)