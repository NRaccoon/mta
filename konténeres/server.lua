local kontenerek = {}

addEvent("onCarSpawnEvent", true)
addEventHandler ( "onCarSpawnEvent", root, function()
    local kocsi = createVehicle(578, 2745.11548, -2448.73755, 13.64844, 0, 0, 270)
    setTimer(function(player)
        warpPedIntoVehicle(player,kocsi, 0)
    end, 50, 1, client)
end)

addEvent("onKontenerAdd", true)
addEventHandler ( "onKontenerAdd", root, function(car)
    local kontener = createObject(2935,getElementPosition(car))
    attachElements(kontener, car, 0, -1.8, 1.1, 0, 0, 0)
    kontenerek[car] = kontener
end)

addEvent("onKontenerLevesz", true)
addEventHandler ( "onKontenerLevesz", root, function(car)
    destroyElement(kontenerek[car])
    setPlayerMoney(source, getPlayerMoney(source)+500)
end)
