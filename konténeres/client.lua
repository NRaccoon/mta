local munkas = createPed(260, 2742.88232, -2453.84668, 13.86226, 270)
local marker = nil
local ures = true
local screenWidth, screenHeight = guiGetScreenSize ()
local timer = nil
local blip = nil

function addTaskOnClick ( button, state, absoluteX, absoluteY, worldX, worldY, worldZ, clickedElement )
        if clickedElement == munkas and state == 'down' then
            local px,py,pz = getElementPosition(localPlayer);
            local x,y,z = getElementPosition(munkas);
            local distance = getDistanceBetweenPoints3D (x, y, z, px, py, pz)
            if(distance < 5) then
                marker = createMarker(2758.94116, -2405.15723, 13.23281,"cylinder")
                blip = createBlip(2758.94116, -2405.15723, 13.23281, 51)
                triggerServerEvent ( "onCarSpawnEvent", root)
            end
        end
end
addEventHandler("onClientClick", root, addTaskOnClick)

function safePed()
	cancelEvent()
end
addEventHandler("onClientPedDamage", munkas, safePed)
addEventHandler("onClientPedHeliKilled", munkas, safePed)
addEventHandler("onClientPedHitByWaterCannon", munkas, safePed)

function playerPressedKey(button, press)
    if (press) and button == 'm' then
        showCursor(not isCursorShowing())
    end
end
addEventHandler("onClientKey", root, playerPressedKey)


function kontenerMarkers(hitElement,matchingDimension)
	if marker == source and getElementType(hitElement) == "player" and getElementModel(hitElement) == 61 and matchingDimension and ures then
        addEventHandler("onClientRender", root, loadingImage)
        timer = setTimer(function()
            removeEventHandler("onClientRender", root, loadingImage)
        end, 2550, 1)
        triggerServerEvent("onKontenerAdd", localPlayer, getPedOccupiedVehicle(localPlayer))
        setCameraViewMode(3)
        destroyElement(marker)
        destroyElement(blip)
        marker = createMarker(2511.39136, -2350.44995, 13.00447,"cylinder")
        blip = createBlip(2511.39136, -2350.44995, 13.00447, 51)
        ures = false
	end
    if marker == source and getElementType(hitElement) == "player" and getElementModel(hitElement) == 61 and matchingDimension and not ures then
        addEventHandler("onClientRender", root, loadingImage)
        timer = setTimer(function()
            removeEventHandler("onClientRender", root, loadingImage)
        end, 2550, 1)
        triggerServerEvent("onKontenerLevesz", localPlayer, getPedOccupiedVehicle(localPlayer))
        destroyElement(marker)
        destroyElement(blip)
        marker = createMarker(2758.94116, -2405.15723, 13.23281,"cylinder")
        blip = createBlip(2758.94116, -2405.15723, 13.23281, 51)
        ures = true
	end
end 
addEventHandler("onClientMarkerHit", resourceRoot, kontenerMarkers)

function loadingImage()
    dxDrawRectangle ( 0, 0, screenWidth, screenHeight, tocolor ( 0, 0, 0, getTimerDetails(timer)/10))
end