local currentMarker = 1;
local currentMarkerElement = createMarker(Vector3(unpack(markers[currentMarker])),"cylinder", 4, markers[currentMarker][4], markers[currentMarker][5], markers[currentMarker][6]);
local screenWidth, screenHeight = guiGetScreenSize ()
local text = "Szeretnél menni még egy kört? (F5)"
local textWidth = dxGetTextWidth (text)
local textHeight = dxGetFontHeight ()
local timer = nil

function nextMarker(hitElement,matchingDimension)
	if (getElementType(hitElement) == "player") then
        currentMarker = currentMarker + 1
        destroyElement(currentMarkerElement)
        if currentMarker > #markers then
            currentMarker = nil
            currentMarkerElement = nil
            timer = setTimer(function() end, 2000, 1)
        else
            currentMarkerElement = createMarker(Vector3(unpack(markers[currentMarker])),"cylinder", 4, markers[currentMarker][4], markers[currentMarker][5], markers[currentMarker][6])
        end
    end 
end 
addEventHandler("onClientMarkerHit", resourceRoot, nextMarker)

function playerPressedKey(button, press)
    if (press) and button == 'F5' and not currentMarker then
        currentMarker = 2;
        currentMarkerElement = createMarker(Vector3(unpack(markers[currentMarker])),"cylinder", 4, markers[currentMarker][4], markers[currentMarker][5], markers[currentMarker][6]);
    end
end
addEventHandler("onClientKey", root, playerPressedKey)


function createText (show)
    if timer and isTimer(timer) then
        dxDrawText ( text , screenWidth / 2 - textWidth / 2, screenHeight / 2 - textHeight / 2)
    end
end
addEventHandler ( "onClientRender", root, createText )

function leaveCar()
    if currentMarkerElement then
        destroyElement(currentMarkerElement)
        currentMarkerElement = nil
    end
end
addEventHandler("onClientPlayerVehicleExit", root, leaveCar)

function enterCar()
	if currentMarker and not currentMarkerElement then
        currentMarkerElement = createMarker(Vector3(unpack(markers[currentMarker])),"cylinder", 4, markers[currentMarker][4], markers[currentMarker][5], markers[currentMarker][6]);
    end
end
addEventHandler("onClientPlayerVehicleEnter", localPlayer, enterCar)