local crops = {}
local saveCrops = {}

function cropLerak(x, y, z, time)
    local pot = createObject(2203, x, y, z+0.2)
    local crop = createObject(861, x, y, z+0.4)
    local tick = time or getTickCount()
    setObjectScale(crop, 0.1)
    setElementData(crop, "lerakas", tick)
    saveCrops[crop] = {x, y, z, tick}
    crops[pot] = crop
end
addEvent("onLerak", true)
addEventHandler("onLerak", root, cropLerak)

function cropHarvest(pot)
    if crops[pot] then
        if getTickCount() - getElementData(crops[pot], "lerakas") > 15000 then
            saveCrops[crops[pot]] = nil
            destroyElement(crops[pot])
            crops[pot] = nil
            destroyElement(pot)
        else 
            outputChatBox("No, no, no, no", client, 255, 0, 0)
        end
    end 
end
addEvent("onHarvest", true)
addEventHandler("onHarvest", root, cropHarvest)

function sendServerTime()
    triggerClientEvent(client, "syncServerTime", source, getTickCount())
end
addEvent("sendServerTime", true)
addEventHandler("sendServerTime", root, sendServerTime)

addEventHandler( "onResourceStop", resourceRoot,
    function()
        local fileHandle = fileCreate("test.json")
        local jsonCrops = {}

        for k,v in pairs(saveCrops) do 
            table.insert(jsonCrops, v)
        end
        if fileHandle then
            fileWrite(fileHandle, toJSON(jsonCrops, false, "tabs"))
            fileClose(fileHandle)
        end
    end
)

addEventHandler ("onResourceStart", resourceRoot, 
    function()
        if not fileExists("test.json") then return end
        local hFile = fileOpen("test.json", true)
        if hFile then
            local buffer
            local text = ""
            while not fileIsEOF(hFile) do
                buffer = fileRead(hFile, 500)
                text = text .. buffer
            end
            fileClose(hFile)
            for k,v in ipairs(fromJSON(text)) do
                cropLerak(unpack(v))
            end
        end
    end 
)