local crops = {}
local serverTick = 0
local fontName = dxCreateFont("Pacifico.ttf", 35)

function syncServerTime(time)
    serverTick = time - getTickCount()
end
addEvent("syncServerTime", true)
addEventHandler("syncServerTime", root, syncServerTime)

function getServerTick()
    return getTickCount() + serverTick
end

function playerPressedKey(button, press)
    if (press) and button == 'm' then
        showCursor (not isCursorShowing())
    end
end
addEventHandler("onClientKey", root, playerPressedKey)

function cropLerak(button, press)
    if (press) and button == 'e' then
        local pos = localPlayer.matrix:transformPosition(Vector3(0, 1.2, 0))
        local fold = getGroundPosition(pos.x, pos.y, pos.z)
        triggerServerEvent("onLerak", root, pos.x, pos.y, fold)
    end 
end
addEventHandler("onClientKey", root, cropLerak)

function harvestCrop(button, state, absoluteX, absoluteY, worldX, worldY, worldZ, clickedElement)
    if clickedElement and state == 'down' and getElementModel(clickedElement) == 2203 then
        triggerServerEvent("onHarvest", localPlayer, clickedElement)
    end
end
addEventHandler("onClientClick", root, harvestCrop)

addEventHandler ( "onClientPreRender", root, function()
    for k, v in pairs(crops) do
        local szazalek = math.min(math.max((getServerTick() - v) / 15000, 0.1), 1)
	    local meret = getEasingValue(szazalek, "OutQuad")
	    setObjectScale(k, meret)

        local ox, oy, oz = getElementPosition(k)
        local px, py, pz = getElementPosition(localPlayer)
        if isElementOnScreen(k) then
            local dis = getDistanceBetweenPoints3D(ox, oy, oz, px, py, pz)
            if dis < 15 then
                local lx, ly = getScreenFromWorldPosition(ox, oy, oz)
                if lx and ly and isLineOfSightClear(ox, oy, oz, px, py, pz, true, true, false, false, false, false, false, localPlayer) then
                    local fontSize = getEasingValue(math.max(math.min((15 - dis) / 15, 1), 0.2), "OutQuad")

                    dxDrawText ( "Doboz: ".. math.ceil(szazalek * 100), lx, ly, lx + 20 , ly + 20, tocolor ( 255, 255, 255, 255 * fontSize ), fontSize, fontSize, fontName,"center", "center", false, false, false, false, true)
                end
            end
        end
    end
end)

addEventHandler( "onClientResourceStart", root, 
    function()
        triggerServerEvent("sendServerTime", localPlayer)

        for k,v in pairs(getElementsByType("object", resourceRoot, true)) do
            local cropTick = getElementData(v, "lerakas")
            if cropTick then
                crops[v] = cropTick
            end
        end
    end
);

addEventHandler("onClientElementStreamIn", resourceRoot,
    function()
        local cropTick = getElementData(source, "lerakas")
        if getElementType(source) == "object" and cropTick then
            crops[source] = cropTick
        end
    end
);

function removeCrop()
    if getElementType(source) == "object" and crops[source] then
        crops[source] = nil
    end
end
addEventHandler( "onClientElementStreamOut", resourceRoot, removeCrop);
addEventHandler("onClientElementDestroy", resourceRoot, removeCrop);