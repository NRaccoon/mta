local rootElement = getRootElement()

local triggered = {}
function runString (commandstring, outputTo, source)
	local sourceName
	if source then
		sourceName = getPlayerName(source)
	else
		sourceName = "Console"
	end
	
	outputDebugString("Runcode: "..sourceName.." lefuttatva "..tostring(commandstring).." (szerver-oldal)", 0)

	function getLocalPlayer( )
		return source
	end
	_G['source'] = source
	if getElementType(source) == 'player' then
		vehicle = getPedOccupiedVehicle(source) or getPedContactElement(source)
		car = vehicle
	end
	settingsSet = set
	settingsGet = get
	p = getPlayerFromName
	c = getPedOccupiedVehicle
	set = setElementData
	get = getElementData
    -- id = function(name) local p = exports.nx_id:findPlayerByPartialNick(localPlayer, name, nil, 2) return p end
    
	local notReturned
	local commandFunction,errorMsg = loadstring("return "..commandstring)
	if errorMsg then
		notReturned = true
		commandFunction, errorMsg = loadstring(commandstring)
	end
	if errorMsg then
		outputChatBoxR("Hiba: "..errorMsg, outputTo)
		return
	end
	results = { pcall(commandFunction) }
	if not results[1] then
		outputChatBoxR("Hiba: "..results[2], outputTo)
		return
	end
	if not notReturned then
		local resultsString = ""
		local first = true
		for i = 2, #results do
			if first then
				first = false
			else
				resultsString = resultsString..", "
			end
			local resultType = type(results[i])
			if isElement(results[i]) then
				resultType = "element:"..getElementType(results[i])
			end
			resultsString = resultsString..inspect(results[i]).." ["..resultType.."]"
		end
		outputChatBoxR("Eredmény: "..resultsString, outputTo)
	elseif not errorMsg then
		outputChatBoxR("Parancs lefuttatva!", outputTo)
	end
end

addCommandHandler("run", function (player, command, ...)
	-- if not exports.nx_perms:has(player, "run") then return end
	local commandstring = table.concat({...}, " ")
	return runString(commandstring, rootElement, player)
end, true)

addCommandHandler("srun", function (player, command, ...)
	-- if not exports.nx_perms:has(player, "srun") then return end
	local commandstring = table.concat({...}, " ")
	return runString(commandstring, player, player)
end, true)

addCommandHandler("crun", function (player, command, ...)
	-- if not exports.nx_perms:has(player, "crun") then return end
	local commandstring = table.concat({...}, " ")
	if player then
		outputDebugString("CRUNcode: "..getPlayerName(player).." lefuttatva "..tostring(commandstring).." (client-oldal)", 0)
		return triggerClientEvent(player, "doCrun", rootElement, commandstring)
	else
		return runString(commandstring, false, false)
	end
end, true)