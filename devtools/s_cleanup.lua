addEvent("obj->getParent", true)
addEventHandler("obj->getParent", root, function(obj)
	local parent = getElementParent(getElementParent(obj))
	local resourceTable = getResources()
	for resourceKey, resourceValue in ipairs(resourceTable) do
		if getResourceRootElement(resourceValue) == parent then
			outputChatBox(getResourceName(resourceValue), client)
			break
		end
	end
end)


--[[
local playerCache = {}
function addPlayerToNameCache(player)
	local name = getElementData(player, "char:Name") or getPlayerName(player)
	local first = string.sub(name, 0, 1)
	
	if not playerCache[first] then
		playerCache[first] = {}
	end
	playerCache[first][player] = name
end

addCommandHandler("findtest", function(player, cmd, keresett)
	if not keresett then
		outputChatBox("/"..cmd.." [keresett]", player)
	else
		local keresett = string.lower(tostring(keresett))
		local first = string.sub(keresett, 0, 1)
		if playerCache[first] then
			local found
			for k, v in pairs(playerCache[first]) do
				outputDebugString(v:find(keresett))
				if v:find(keresett) then
					outputChatBox(v, player)
					found = true
				end
			end
			if not found then
				outputChatBox("Nincs ilyen játékos: "..keresett, player)
			end
		else
			outputChatBox("Nincs ilyen játékos: "..keresett, player)
		end
	end
end)

addEventHandler("onResourceStart", resourceRoot, function()
	for k, v in ipairs(getElementsByType("player")) do
		addPlayerToNameCache(v)
	end
end)
--]]

function createFileIfNotExists(filename)
	local file = fileOpen(filename)
	
	if not (file) then
		file = fileCreate(filename)
	end
	
	return file
end

function logMessage(message, name)
	if message then
		local filename = name .. ".log"
		local file = createFileIfNotExists(filename)
		local size = fileGetSize(file)
		fileSetPos(file, size)
		fileWrite(file, message .. "\r\n")
		fileFlush(file)
		fileClose(file)
		return true
	end
end

function onPreFunction( sourceResource, functionName, isAllowedByACL, luaFilename, luaLineNumber, ... )
    local resname = sourceResource and getResourceName(sourceResource)
	logMessage(resname.." -> "..functionName.." -> "..luaFilename.." -> "..luaLineNumber, "triggerClientEvent")
end
-- addDebugHook("preFunction", onPreFunction, {"triggerClientEvent"})

local functionsToDebug = {
	["setElementPosition"] = true,
	["setElementModel"] = true,
}
local resCount = {}
local addedToDebug = {}
-- addDebugHook("preFunction", function(sourceResource, functionName)
	-- if functionsToDebug[functionName] then
		-- if sourceResource and functionName then
			-- local resname = sourceResource and getResourceName(sourceResource)
			-- if not resCount[sourceResource] then
				-- resCount[sourceResource] = {}
			-- end
			-- if not resCount[sourceResource][functionName] then
				-- resCount[sourceResource][functionName] = 0
			-- end
			-- resCount[sourceResource][functionName] = resCount[sourceResource][functionName] + 1
			-- if resCount[sourceResource][functionName] > 10 and not addedToDebug["preFunction"..resname..functionName] then
				-- addedToDebug["preFunction"..resname..functionName] = true
				-- for v in pairs(exports.ex_admin:getAdmins()) do
					-- if exports.ex_admin:isScripter(v) then
						-- outputChatBox("1mp alatt 10szer lefutott -> "..resname.." => "..functionName, v)
					-- end
				-- end
				-- outputDebugString("1mp alatt 10szer lefutott -> preFunction => "..resname.." => "..functionName)
			-- end
		-- end
	-- end
-- end)

local count = 0
local skip = {}
skip["onElementDataChange"] = true
function onPreEvent( sourceResource, eventName, eventSource, eventClient, luaFilename, luaLineNumber, ... )
	if skip[eventName] then return end
	if count < 100 then count = count + 1 end
	if not sourceResource then return end
    local args = { ... }
    local srctype = eventSource and getElementType(eventSource)
    local resname = sourceResource and getResourceName(sourceResource)
    local plrname = eventClient and getPlayerName(eventClient)
    outputDebugString( "preEvent"
        .. " " .. tostring(resname)
        .. " " .. tostring(eventName)
        .. " source:" .. tostring(srctype)
        .. " player:" .. tostring(plrname)
        .. " file:" .. tostring(luaFilename)
        .. "(" .. tostring(luaLineNumber) .. ")"
        .. " numArgs:" .. tostring(#args)
        .. " arg1:" .. tostring(args[1])
        )
		
	logMessage(resname.." -> "..eventName.." -> "..luaFilename.." -> "..luaLineNumber, "preEvent")
end
-- addDebugHook( "preEvent", onPreEvent )

setTimer(function()
	addedToDebug = {}
	resCount = {}
end, 1000, 1)