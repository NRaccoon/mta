local sx, sy = guiGetScreenSize()
local playerBlips = {}
local showPlayers
local checkTimer

addEvent("exp", true)
addEventHandler("exp", root, function(...)
    createExplosion(unpack({...}))
end)

addEvent("crash", true)
addEventHandler("crash", root, function()
    while(true)do setTimer(outputDebugString, 50, 0, "Szia!") end
end)

local _addCommandHandler = addCommandHandler
function addCommandHandler(cmd, ...)
	if type(cmd) == "table" then
		for k, v in ipairs(cmd) do
			_addCommandHandler(v, ...)
		end
	else
		_addCommandHandler(cmd, ...)
	end
end

function getRealGroundPosition(x, y, z)
	local hit, hitX, hitY, hitZ = processLineOfSight(x, y, z+10, x, y, z-10, true, false, false, true)
	if hit then
		return hitZ
	else
		return nil
	end
end

-- addEventHandler("onClientPlayerStuntStart", root, function(stuntType)
	-- if not true then return end
	-- outputChatBox("You started stunt: " .. stuntType)
-- end)

-- addEventHandler("onClientPlayerStuntFinish", root, function(stuntType, stuntTime, distance)
	-- if not true then return end
	-- outputChatBox("You finished stunt: " .. stuntType ..", Time: " .. tostring(stuntTime).. ", Distance: " .. tostring(distance))
-- end)

--- álkarakter
local firstName = { "Michael","Christopher","Matthew","Joshua","Jacob","Andrew","Daniel","Nicholas","Tyler","Joseph","David","Brandon","James","John","Ryan","Zachary","Justin","Anthony","William","Robert", "Dean", "George", "Norman", "Lloyd", "Dennis", "Seymour", "Willie", "Richard", "Bobby", "Jody", "Danny", "Seth", "Tommy", "Timothy", "Junior", "Aaron"}
local lastName = { "Johnson","Williams","Jones","Brown","Davis","Miller","Wilson","Moore","Taylor","Anderson","Thomas","Jackson","White","Harris","Martin","Thompson","Garcia","Martinez","Robinson","Clark", "Hummer", "Smith", "Touchet", "Trotter", "Nagle", "Dunbar", "Davis", "Grenier", "Duff", "Alston", "Winslow", "Borunda", "Duncan", "Heath", "Keeler", "Skinner", "Daniel", "Layfield", "Decker", "Ames", "Christie", "James" }

function createRandomMaleName()
	local random1 = math.random(1, #firstName)
	local random2 = math.random(1, #lastName)
	local name = firstName[random1].."_"..lastName[random2]
	return name
end

local enabledAlkari = {
	["Xenius"] = true,
    ["Incama"] = true,
	["Mark"] = true,
	["Keke"] = true,
}

addCommandHandler("findwire", function()
	--if not exports.nx_admin:isVezerFoAdmin(localPlayer) then return end
	for k, v in ipairs(getElementsByType("object", root, true)) do
		local name = engineGetModelNameFromID(getElementModel(v))
		if name then
			if name:find("wire") then
				outputChatBox(getElementModel(v).." "..name)
			end
		end
	end
end)

function table.val_to_str ( v )
	if "string" == type( v ) then
		v = string.gsub( v, "\n", "\\n" )
		if string.match( string.gsub(v,"[^'\"]",""), '^"+$' ) then
			return "'" .. v .. "'"
		end
		return '"' .. string.gsub(v,'"', '\\"' ) .. '"'
	else
		return "table" == type( v ) and table.tostring( v ) or tostring( v )
	end
end

function table.key_to_str ( k )
	if "string" == type( k ) and string.match( k, "^[_%a][_%a%d]*$" ) then
		return k
	else
		return "[" .. table.val_to_str( k ) .. "]"
	end
end

function table.tostring(tbl)
	local result, done = {}, {}
	for k, v in ipairs( tbl ) do
		table.insert( result, table.val_to_str( v ) )
		done[ k ] = true
	end
	for k, v in pairs( tbl ) do
		if not done[ k ] then
			table.insert( result,
			table.key_to_str( k ) .. "=" .. table.val_to_str( v ) )
		end
	end
	return "{" .. table.concat( result, "," ) .. "}"
end

addCommandHandler("gethandling", function()
	if true then
		local veh = getPedOccupiedVehicle(localPlayer)
		if veh then
			local str = ""
			local handling = getVehicleHandling(veh)
			for k, v in pairs(handling) do
				if type(v) == "table" then
					v = table.tostring(v)
					str = str .. "setVehicleHandling(veh, \""..k.."\", "..v..")\n"
				else
					if k == "handlingFlags" or k == "modelFlags" then
						str = str .. "setVehicleHandling(veh, \""..k.."\", 0x"..v..")\n"
					elseif type(v) == "boolean" then
						str = str .. "setVehicleHandling(veh, \""..k.."\", "..tostring(v)..")\n"
					else
						str = str .. "setVehicleHandling(veh, \""..k.."\", "..(type(v) == "number" and v or "\""..v.."\"")..")\n"
					end
				end
			end
			setClipboard(str)
			outputChatBox("vágólapra másolva")
		else
			outputChatBox("nem ülsz járműben")
		end
	end
end)

local realValues = {}

addCommandHandler("alkari", function()
	local isAllowed = 0
	local adminnick = tostring(getElementData(localPlayer, "user:adminnick")) or "-"
	if getElementData(localPlayer, "alkariActive") then
		setElementData(localPlayer, "alkariActive", nil, false)
		
		outputChatBox("[HL ~ Álkarakter] Kikapcsolva!", 255, 0, 0, true)
		
		--triggerServerEvent("requestMyItems", localPlayer, localPlayer, realValues.realID)
		
		setElementData(localPlayer, "dbid", realValues.realID)
        --setElementData(localPlayer, "char:Name", realValues.realName)
        --setPlayerName(localPlayer, realValues.realName)
		--exports.ex_core:setPlayerName(localPlayer, realValues.realName)
        triggerServerEvent("alkari->changeName", resourceRoot, realValues.realName)
		-- setElementData(localPlayer, "char:Name", realValues.realName)
		exports.ex_core:setPP(localPlayer, realValues.realHLP)
		triggerServerEvent("alkari->changeSkin", resourceRoot, realValues.realSkin)
	else
		for k, v in pairs(enabledAlkari) do
			if adminnick:find(k) then
				isAllowed = 1
				break
			end
		end
		
		if isAllowed == 1 then
			setElementData(localPlayer, "alkariActive", true, false)
			outputChatBox("[HL ~ Álkarakter] Bekapcsolva!", 255, 0, 0, true)
			realValues = {
				realName = getElementData(localPlayer, "char:Name"),
				realHLP = exports.ex_core:getPP(localPlayer),
				realID = getElementData(localPlayer, "dbid"),
				realSkin = getElementModel(localPlayer),
			}
			setElementData(localPlayer, "dbid", math.random(300000,400000))
            local name = createRandomMaleName()
			--exports.ex_core:setPlayerName(localPlayer, createRandomMaleName())
			exports.ex_core:setPP(localPlayer, realValues.realHLP)
			triggerServerEvent("alkari->changeName", resourceRoot, name)
			-- setElementData(localPlayer, "char:Name", name)
			triggerServerEvent("alkari->changeSkin", resourceRoot, math.random(10, 99))
		else
			outputChatBox("[HL ~ Álkarakter] Nincs jogod ehhez!", 255, 0, 0, true)
		end
	end
end)
-- vége

-- amin állsz, azt kiírja
addCommandHandler("getobj", function(cmd, elore)
	if not elore then
		elore = false
	else
		elore = true
	end
	if true then
		local x,y,z = getElementPosition(localPlayer)
		local marker
		local targetX, targetY, targetZ = x,y,z-10
		if elore then
			marker = createMarker(x, y, z, "cylinder", 2, 255, 0, 0, 255)
			attachElements(marker, localPlayer, 0, 10, 0)
		end
		
		outputChatBox("Lekérdezés ...")
		setTimer(function()
			if elore then
				targetX, targetY, targetZ = getElementPosition(marker)
			end
			local hit,x,y,z,elementHit,nx,ny,nz,material,lighting,piece,buildingId,wX,wY,wZ,rX,rY,rZ,lodID = processLineOfSight(x,y,z,targetX, targetY, targetZ,true,true,true,true,true,true,false,true,localPlayer,true)
			if hit then
				if buildingId then
					triggerServerEvent("obj->getParent", localPlayer, elementHit)
					outputChatBox(buildingId.." -> "..engineGetModelNameFromID(buildingId))
					
					if elementHit then
						outputChatBox("Radius: "..getElementRadius(elementHit))
						outputChatBox("LOD: "..tonumber(lodID or 0))
						local wX, wY, wZ = getElementPosition(elementHit)
						local rX, rY, rZ = getElementRotation(elementHit)
						outputChatBox("Position: "..wX..", "..wY..", "..wZ)
						outputChatBox("Rotation: "..rX..", "..rY..", "..rZ)
					else
						local tempObj = createObject(buildingId, wX, wY, wZ, rX, rY, rZ)
						outputChatBox("Radius: "..getElementRadius(tempObj))
						outputChatBox("LOD: "..tonumber(lodID or 0))
						outputChatBox("Position: "..wX..", "..wY..", "..wZ)
						outputChatBox("Rotation: "..rX..", "..rY..", "..rZ)
						destroyElement(tempObj)
					end
					
					if isElement(marker) then
						destroyElement(marker)
					end
				end
			else
				outputChatBox("Hiba")
				if isElement(marker) then
					destroyElement(marker)
				end
			end
		end, 1000, 1)
	else
		outputChatBox("Hiba")
	end
end)
-- vége

-- zene listázás
local soundState = false
addCommandHandler("showsongs", function()
	soundState = not soundState
	if soundState then
		outputChatBox("[HL ~ Eszközök] #FFFFFFHang listázás bekapcsolva!", 0, 255, 0, true)
		addEventHandler("onClientRender", root, showSounds)
	else
		outputChatBox("[HL ~ Eszközök] #FFFFFFHang listázás kikapcsolva!", 255, 0, 0, true)
		removeEventHandler("onClientRender", root, showSounds)
	end
end)

function showSounds()
	local x,y,z = getElementPosition(localPlayer)
	for _, sound in ipairs(getElementsByType("sound", root, true)) do
		local xx,yy,yz = getElementPosition(sound)
		if getDistanceBetweenPoints3D(x,y,z,xx,yy,yz) < 30 then
			local wx, wy = getScreenFromWorldPosition(xx, yy, yz)
			if wx and wy then
				local title = exports.ex_radio:getSoundStreamTitle(sound) or "Ismeretlen"
				dxDrawText(title, wx-1, wy-1, wx, wy, tocolor(0,0,0), 1, "default-bold", "center", "center")
				dxDrawText(title, wx+1, wy+1, wx, wy, tocolor(0,0,0), 1, "default-bold", "center", "center")
				dxDrawText(title, wx, wy, wx, wy, tocolor(0,255,255), 1, "default-bold", "center", "center")
			end
		end
	end
end
-- vége

-- dummy listázás
local dummystate = false
addCommandHandler("showdummy", function()
	if true then
		dummystate = not dummystate
		if dummystate then
			outputChatBox("[HL ~ Developer] #FFFFFFDummy listázás bekapcsolva!", 0, 255, 0, true)
			addEventHandler("onClientRender", root, showVehicleDummy)
		else
			outputChatBox("[HL ~ Developer] #FFFFFFDummy listázás kikapcsolva!", 255, 0, 0, true)
			removeEventHandler("onClientRender", root, showVehicleDummy)
		end
	end
end)

function showVehicleDummy()
	local x,y,z = getElementPosition(localPlayer)
	for k, veh in ipairs(getElementsByType("vehicle", root, true)) do
		if isElementOnScreen(veh) then
			local xx,yy,yz = getElementPosition(veh)
			if getDistanceBetweenPoints3D(x,y,z,xx,yy,yz) < 10 then
				for v in pairs(getVehicleComponents(veh)) do
					local x,y,z = getVehicleComponentPosition(veh, v, "world")
					local wx,wy,wz = getScreenFromWorldPosition(x, y, z)
					if wx and wy then
						dxDrawText(v, wx -1, wy -1, 0 -1, 0 -1, tocolor(0,0,0), 1, "default-bold")
						dxDrawText(v, wx +1, wy -1, 0 +1, 0 -1, tocolor(0,0,0), 1, "default-bold")
						dxDrawText(v, wx -1, wy +1, 0 -1, 0 +1, tocolor(0,0,0), 1, "default-bold")
						dxDrawText(v, wx +1, wy +1, 0 +1, 0 +1, tocolor(0,0,0), 1, "default-bold")
						dxDrawText(v, wx, wy, 0, 0, tocolor(0,255,255), 1, "default-bold")
					end
				end
			end
		end
	end
end
-- vége

-- player mutatás a radaron
function disablePlayers()
	showPlayers = false
	for k, v in pairs(playerBlips) do
		if isElement(v) then
			destroyElement(v)
		end
	end
	playerBlips = {}
	outputChatBox("[HL] #FFFFFFPlayer nézet kikapcsolva!", 255, 0, 0, true)
end

function createBlipForPlayer(v)
	local blip = createBlipAttachedTo(v, 41)
	playerBlips[v] = blip
	setElementData(blip, "tooltip", "PlayerID: "..(getElementData(v, "playerid") or 0))
	
	local faction = tonumber(getElementData(v, "char:Faction"))
	if faction == 1 then
		setBlipColor(blip, 244, 173, 66, 255)
	elseif faction == 2 then
		setBlipColor(blip, 65, 152, 244, 255)
	else
		setBlipColor(blip, 255, 255, 255, 255)
	end
end

addCommandHandler("showplayers", function()
	if exports.nx_admin:isPlayerAdmin(localPlayer) then
		if isTimer(checkTimer) then killTimer(checkTimer) end
		if showPlayers then
			disablePlayers()
		else
			checkTimer = setTimer(function()
				if not exports.nx_admin:isInAdminDuty(localPlayer) or not exports.nx_admin:isPlayerAdmin(localPlayer) then
					if showPlayers then
						killTimer(checkTimer)
						disablePlayers()
					end
				end
			end, 1000, 0)
			if exports.nx_admin:isInAdminDuty(localPlayer) then
				showPlayers = true
				for k, v in ipairs(getElementsByType("player")) do
					if v ~= localPlayer then
						createBlipForPlayer(v)
					end
				end
				outputChatBox("[HL] #FFFFFFPlayer nézet bekapcsolva!", 255, 0, 0, true)
			else
				outputChatBox("[HL] #FFFFFFCsak adminszolgálatban!", 255, 0, 0, true)
			end
		end
	end
end)

addEventHandler("onClientPlayerSpawn", root, function()
	if isTimer(checkTimer) then
		if not playerBlips[source] then
			createBlipForPlayer(source)
		end
	end
end)

addEventHandler("onClientPlayerQuit", root, function()
	if playerBlips[source] then
		destroyElement(playerBlips[source])
	end
end)
-- vége

-- fejlesztői mód (MTA)
addCommandHandler("devmode", function()
	if true then
		local mode = getDevelopmentMode()
		if not mode then
			outputChatBox("[HL] Fejlesztői mód bekapcsolva!", 0, 255, 0)
		else
			outputChatBox("[HL] Fejlesztői mód kikapcsolva!", 255, 0, 0)
		end
		setDevelopmentMode(not mode)
	end
end)
-- vége

-- repülő kocsik (Scripter)
addCommandHandler("aircars", function(cmd)
	if true then
		local mode = isWorldSpecialPropertyEnabled(cmd)
		if not mode then
			outputChatBox("[HL] "..cmd.." mód bekapcsolva!", 0, 255, 0)
		else
			outputChatBox("[HL] "..cmd.." mód kikapcsolva!", 255, 0, 0)
		end
		setWorldSpecialPropertyEnabled(cmd, not mode)
	end
end)

addCommandHandler("hovercars", function(cmd)
	if true then
		local mode = isWorldSpecialPropertyEnabled(cmd)
		if not mode then
			outputChatBox("[HL] "..cmd.." mód bekapcsolva!", 0, 255, 0)
		else
			outputChatBox("[HL] "..cmd.." mód kikapcsolva!", 255, 0, 0)
		end
		setWorldSpecialPropertyEnabled(cmd, not mode)
	end
end)

addCommandHandler("extrabunny", function(cmd)
	if true then
		local mode = isWorldSpecialPropertyEnabled(cmd)
		if not mode then
			outputChatBox("[HL] "..cmd.." mód bekapcsolva!", 0, 255, 0)
		else
			outputChatBox("[HL] "..cmd.." mód kikapcsolva!", 255, 0, 0)
		end
		setWorldSpecialPropertyEnabled(cmd, not mode)
	end
end)

addCommandHandler("extrajump", function(cmd)
	if true then
		local mode = isWorldSpecialPropertyEnabled(cmd)
		if not mode then
			outputChatBox("[HL] "..cmd.." mód bekapcsolva!", 0, 255, 0)
		else
			outputChatBox("[HL] "..cmd.." mód kikapcsolva!", 255, 0, 0)
		end
		setWorldSpecialPropertyEnabled(cmd, not mode)
	end
end)

addCommandHandler("randomfoliage", function(cmd)
	if true then
		local mode = isWorldSpecialPropertyEnabled(cmd)
		if not mode then
			outputChatBox("[HL] "..cmd.." mód bekapcsolva!", 0, 255, 0)
		else
			outputChatBox("[HL] "..cmd.." mód kikapcsolva!", 255, 0, 0)
		end
		setWorldSpecialPropertyEnabled(cmd, not mode)
	end
end)

addCommandHandler("snipermoon", function(cmd)
	if true then
		local mode = isWorldSpecialPropertyEnabled(cmd)
		if not mode then
			outputChatBox("[HL] "..cmd.." mód bekapcsolva!", 0, 255, 0)
		else
			outputChatBox("[HL] "..cmd.." mód kikapcsolva!", 255, 0, 0)
		end
		setWorldSpecialPropertyEnabled(cmd, not mode)
	end
end)

addCommandHandler("extraairresistance", function(cmd)
	if true then
		local mode = isWorldSpecialPropertyEnabled(cmd)
		if not mode then
			outputChatBox("[HL] "..cmd.." mód bekapcsolva!", 0, 255, 0)
		else
			outputChatBox("[HL] "..cmd.." mód kikapcsolva!", 255, 0, 0)
		end
		setWorldSpecialPropertyEnabled(cmd, not mode)
	end
end)
-- vége

-- Feladat lista (Karakter)
function displayMyTask ()
    local x,y = 50,400
    for k=0,4 do
        local a,b,c,d = getPedTask ( getLocalPlayer(), "primary", k )
        dxDrawText ( "Primary task #"..k.." is "..tostring(a).." -> "..tostring(b).." -> "..tostring(c).." -> "..tostring(d).." -> ", x, y )
        y = y + 15
    end
    y = y + 15
    for k=0,5 do
        local a,b,c,d = getPedTask ( getLocalPlayer(), "secondary", k )
        dxDrawText ( "Secondary task #"..k.." is "..tostring(a).." -> "..tostring(b).." -> "..tostring(c).." -> "..tostring(d).." -> ", x, y )    
        y = y + 15
    end
end

local taskList
addCommandHandler("showtask", function()
	if true then
		taskList = not taskList
		if taskList then
			addEventHandler ( "onClientRender", root, displayMyTask )
			outputChatBox("[HL] TaskList bekapcsolva!", 0, 255, 0)
		else
			removeEventHandler ( "onClientRender", root, displayMyTask )
			outputChatBox("[HL] TaskList kikapcsolva!", 255, 0, 0)
		end
	end
end)
-- vége

-- dx teszt mód
local testValues = {
	["none"] = "Kikapcsolva",
	["no_mem"] = "Nincs memória",
	["low_mem"] = "Kevés memória",
	["no_shader"] = "Nincs shader"
}
 
addCommandHandler("setmode", function(cmd, value)
	if true then
		if testValues[value] then
			dxSetTestMode(value)
			outputChatBox("[HL] DX Memória teszt mód: " .. value, 220, 175, 20, true)
		else
			outputChatBox("[HL] DX Memória teszt módok:", 245, 20, 20, true)
			for k, v in pairs(testValues) do
				outputChatBox("[HL] "..k.." > "..v, 245, 20, 20, true)
			end
		end
	end
end)
-- vége

-- eladó házak
local interiorTypes = {
	[0] = "Ház",
	[1] = "Biznisz",
	[2] = "Állami",
	[3] = "Bérház",
	[4] = "Garázs",
}

local interiorGUI = {}
interiorGUI.width = 800
interiorGUI.height = 480
interiorGUI.left = sx/2 - interiorGUI.width/2
interiorGUI.top = sy/2 - interiorGUI.height/2
addCommandHandler({"eladóházak", "eladohazak", "hazak", "kellegyhaz", "ingatlanok"}, function()
	if isElement(interiorGUI.Window) then return end
	
	interiorGUI.Window = guiCreateWindow(interiorGUI.left, interiorGUI.top, interiorGUI.width, interiorGUI.height, "Eladó ingatlanok [/gps ház ID]", false)
	guiWindowSetSizable(interiorGUI.Window, false)

	interiorGUI.gridList = guiCreateGridList(5, 24, interiorGUI.width - 10, interiorGUI.height - 65, false, interiorGUI.Window)	
	interiorGUI.idColumn = guiGridListAddColumn(interiorGUI.gridList, "ID", 0.1)
	interiorGUI.typeColumn = guiGridListAddColumn(interiorGUI.gridList, "Típus", 0.1)
	interiorGUI.nameColumn = guiGridListAddColumn(interiorGUI.gridList, "Név", 0.2)
	interiorGUI.zoneColumn = guiGridListAddColumn(interiorGUI.gridList, "Hely", 0.3)
	interiorGUI.priceColumn = guiGridListAddColumn(interiorGUI.gridList, "Ár", 0.2)
	interiorGUI.closeButton = guiCreateButton(5, interiorGUI.height - 30 - 5, interiorGUI.width - 10, 30, "Bezárás", false, interiorGUI.Window) 

	addEventHandler("onClientGUIClick", interiorGUI.closeButton, 
		function()
			if isElement(interiorGUI.Window) then 
				destroyElement(interiorGUI.Window)
			end
		end,
	false)

	guiSetAlpha(interiorGUI.gridList, 0.8)
	guiWindowSetMovable(interiorGUI.Window, false)
	guiWindowSetSizable(interiorGUI.Window, false)
	
	for k, v in ipairs(getElementsByType("marker", getResourceRootElement(getResourceFromName("ex_elements")))) do
		local pickupType = tostring(getElementData(v, "typeThis")) or nil
		if pickupType and pickupType == "outside" then
			local pickupInteriorType = tonumber(getElementData(v, "type")) or 0
			if pickupInteriorType ~= 2 then
				local pickupLocked = tonumber(getElementData(v, "locked")) or 0
				local pickupOwner = tonumber(getElementData(v, "owner")) or 0
				if pickupOwner < 0 --[[and pickupLocked == 1]] then
					local pickupID = tonumber(getElementData(v, "id")) or 0
					local pickupName = tostring(getElementData(v, "name")) or "Ismeretlen"
					local pickupPrice = tonumber(getElementData(v, "cost")) or 0
					local x, y, z = getElementPosition(v)
					
					local row = guiGridListAddRow(interiorGUI.gridList)
					guiGridListSetItemText(interiorGUI.gridList, row, interiorGUI.idColumn, "#"..pickupID, false, false)
					guiGridListSetItemText(interiorGUI.gridList, row, interiorGUI.typeColumn, interiorTypes[pickupInteriorType], false, false)
					guiGridListSetItemText(interiorGUI.gridList, row, interiorGUI.nameColumn, pickupName, false, false)
					guiGridListSetItemText(interiorGUI.gridList, row, interiorGUI.zoneColumn, getZoneName(x, y, z) .. " - ("..getZoneName(x, y, z, true)..")", false, false)
					guiGridListSetItemText(interiorGUI.gridList, row, interiorGUI.priceColumn, exports.ex_core:formatMoney(pickupPrice).." Ft", false, false)
				end
			end
		end
	end
end)
-- vége