local cars = {558, 559, 560, 561, 562}
local objects = {3471, 1224, 1608, 1609, 1607}
local kocsik = {}
local objektek = {}

function spawner ()
    local pos = client.matrix:transformPosition(Vector3(2, 0, 0))
    local rx, ry, rz = getElementRotation(client)
    local item = nil
    if (math.random(2) == 1) then
        item = createVehicle(cars[math.random(#cars)], pos.x, pos.y, pos.z, Vector3(rx, ry, rz))
        table.insert(kocsik, item)
    else
        item = createObject(objects[math.random(#objects)], pos.x, pos.y, pos.z, Vector3(rx, ry, rz ))
        table.insert(objektek, item)
    end
    setElementDimension(item, getElementDimension(client))
    setElementInterior(item, getElementInterior(client))
end
addEvent("onLerak", true)
addEventHandler("onLerak", root, spawner)

function  eltuntet()
    local px,py,pz = getElementPosition(client)
    local nearest = nil
    local prevdis = 10000000
    local isCar = true
    for k,v in pairs(kocsik) do 
        local vx,vy,vz = getElementPosition(v)
		local dis = getDistanceBetweenPoints3D(px,py,pz,vx,vy,vz)
        if dis < prevdis then
            prevdis = dis 
            nearest = k
        end
    end
    for k,v in pairs(objektek) do 
        local vx,vy,vz = getElementPosition(v)
		local dis = getDistanceBetweenPoints3D(px,py,pz,vx,vy,vz)
        if dis < prevdis then
            prevdis = dis 
            nearest = k
            isCar = false
        end
    end
    if isCar and kocsik[nearest] then
        destroyElement(kocsik[nearest])
        table.remove(kocsik, nearest)
    elseif objektek[nearest] then
        destroyElement(objektek[nearest])
        table.remove(objektek, nearest)
    end
end
addEvent("onEltuntet", true)
addEventHandler("onEltuntet", resourceRoot, eltuntet)