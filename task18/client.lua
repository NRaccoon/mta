local screenWidth, screenHeight = guiGetScreenSize ( )
local fontName = dxCreateFont("Pacifico.ttf", 30)
local timer = nil
local text = nil
local boxInHand = false
local marker = createMarker(2486.36230, -1667.69238, 12.34375, "cylinder", 2)
local colshapeElements = {}

function cursorShow(button, press)
    if (press) and button == 'm' then
        showCursor (not isCursorShowing())
    end 
end
addEventHandler("onClientKey", root, cursorShow)

function clickOnBox ( button, state, absoluteX, absoluteY, worldX, worldY, worldZ, clickedElement)
    if clickedElement and state == 'down' then
        triggerServerEvent("onFelvesz", localPlayer, clickedElement)
    end
end
addEventHandler("onClientClick", root, clickOnBox)

function boxLerak(hitPlayer, matchingDimension)
    if boxInHand then
	    triggerServerEvent("onLerak", localPlayer)
    end
end
addEventHandler ("onClientMarkerHit", root, boxLerak)

function kiir(szoveg)
    text = szoveg
    timer = setTimer(function() end, 2000, 1)
end
addEvent("onKiir", true)
addEventHandler("onKiir", root, kiir)

function createText ()
    if timer and isTimer(timer) then
        local textWidth = dxGetTextWidth (text, 1, fontName)
        dxDrawText ( text, screenWidth / 2 - textWidth / 2, 20, textWidth, 20, tocolor ( 255, 255, 255, 255 ), 1, 1, fontName)
    end
    for k, v in pairs(colshapeElements) do
        local lx, ly, lz = getScreenFromWorldPosition(getElementPosition(k))
        if lx and ly and lz then
            dxDrawText ( "Doboz: "..v, lx, ly, lx + 20 , ly + 20, tocolor ( 255, 255, 255, 255 ), 1, 1, fontName,"center", "center")
        end
    end
end
addEventHandler ( "onClientRender", root, createText )

function boxInHandE(bx, by, bz)
    boxInHand = not boxInHand
end
addEvent("onBoxHand", true)
addEventHandler("onBoxHand", root, boxInHandE)


function scoreChangeTracker(theKey, oldValue, newValue)
    if (getElementType(source) == "colshape") and (theKey == "count") then
        -- outputChatBox(getPlayerName(source).."'s new score is "..newValue.."!")
        colshapeElements[source] = newValue
    end
end
addEventHandler("onClientElementDataChange", root, scoreChangeTracker)

addEventHandler( "onClientResourceStart", resourceRoot,
    function ()
        local colshapes = getElementsByType("colshape", resourceRoot,true)
        for theKey, colShape in ipairs(colshapes) do
            colshapeElements[colShape] = 0
        end
    end
);