local controls = {'fire', 'aim_weapon', 'next_weapon', 'previous_weapon', 'jump', 'sprint', 'crouch', 'enter_exit'}
local colshapes = {}
local boxInHand = nil
local timer = nil

function createBox()
    for k,v in pairs(boxPos) do
        local box = createObject(1221, Vector3(boxPos[k]))
        local col = createColSphere(Vector3(v), 3)
        setObjectScale (box, 0.5)
        setElementFrozen (box, true)
        colshapes[col] = box
    end
end
addEventHandler ("onResourceStart", root, createBox)

function boxFelvesz(box)
    local px, py, pz = getElementPosition(client)
    for k,v in pairs(colshapes) do
        if isInsideColShape(k, px, py, pz) and v == box then
            setElementFrozen (box, false)
            exports.bone_attach:attachElementToBone(box, client, 11, -0.2, 0.2, 0.13, 270, 20, 35);
            boxInHand = box
            triggerClientEvent("onBoxHand", root)
            setPedAnimation(client, "carry", "crry_prtial", 0, false, true, true, true)
            for k, v in pairs(controls) do
                toggleControl (client, v, false)
            end
            if v == boxInHand then 
                colshapes[k] = false
            end
            local player = client
            timer = setTimer(function()
                setPedAnimation(player, "RIOT", "RIOT_ANGRY", -1, false, false, false, false)
                destroyElement(boxInHand)
                for k, v in pairs(controls) do
                    toggleControl (source, v, true)
                end
                boxInHand = nil
                triggerClientEvent("onBoxHand", root)
                triggerClientEvent("onKiir", player, "Szerencsétlen, ez nem sikerült!")
            end, 5000, 1)
        end
    end
end
addEvent("onFelvesz", true)
addEventHandler("onFelvesz", root, boxFelvesz)

function boxLerak()
    setPedAnimation(client, "carry", "putdwn", -1, false, false, false, false)
    destroyElement(boxInHand)
    for k, v in pairs(controls) do
        toggleControl (source, v, true)
    end
    boxInHand = nil
    triggerClientEvent("onBoxHand", root)
    killTimer(timer)
end
addEvent("onLerak", true)
addEventHandler("onLerak", root, boxLerak)


setTimer(function()
    for k,v in pairs(colshapes) do
        if not colshapes[k] then
            local x, y, z = getElementPosition(k)
            local box = createObject(1221, x, y, z)
            setObjectScale (box, 0.5)
            setElementFrozen (box, true)
            colshapes[k] = box
            setElementData(k, "count", (getElementData(k, "count") or 0) +1)
        end
    end
end, 10000, 0)