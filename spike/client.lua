local tusik = {}
local cols = {}
local bigCols = {}
local fontName = dxCreateFont("Pacifico.ttf", 35)
local show = false
local kocsikInCol = {}

addCommandHandler("spike", function()
    local pos = localPlayer.matrix:transformPosition(Vector3(0, 6, 0))
    local pz = getGroundPosition(pos.x, pos.y, pos.z)
    local rx, ry, rz = getElementRotation(localPlayer)
    triggerServerEvent("onSpikeLerak", localPlayer, pos.x, pos.y, pz, rx, ry, rz)
    local x = pos.x
    local y = pos.y
    local width = 5
    local height = 0.5

    local topX = x + width * math.cos(math.rad(rz - 90))
    local topY = y + width * math.sin(math.rad(rz - 90))
    local bottomX = x + width * math.cos(math.rad(rz + 90))
    local bottomY = y + width * math.sin(math.rad(rz + 90))

    local topRightX = topX + height * math.cos(math.rad(90))
    local topRightY = topY + height * math.sin(math.rad(90))
    local topLeftX = topX + height * math.cos(math.rad(270))
    local topLeftY = topY + height * math.sin(math.rad(270))

    local bottomRightX = bottomX + height * math.cos(math.rad(90))
    local bottomRightY = bottomY + height * math.sin(math.rad(90))
    local bottomLeftX = bottomX + height * math.cos(math.rad(270))
    local bottomLeftY = bottomY + height * math.sin(math.rad(270))

    local kCol = createColSphere(pos.x, pos.y, pos.z, 7.5)

    local col = createColPolygon(
        x, y,
        topRightX, topRightY, -- top right
        topLeftX, topLeftY, -- top left
        bottomLeftX, bottomLeftY, -- bottom left
        bottomRightX, bottomRightY -- bottom right

    )
    setColPolygonHeight(col, pz, pz+1)
    bigCols[kCol] = col
    table.insert(cols, col)
end)

addCommandHandler("nearbyspikes", function()
    if show then 
        removeEventHandler ( "onClientRender", root, showSpikes)
        show = false
    else
        triggerServerEvent("onShowSpike", localPlayer)
        show = true
        addEventHandler ( "onClientRender", root, showSpikes)
    end
end)

addCommandHandler("delspike", function(commandName, sorszam)
    if sorszam then
        local id = tonumber(sorszam)
        triggerServerEvent("onDelSpike", localPlayer, id)
        tusik[id] = nil
    else
        outputChatBox("Add meg a törölni kívánt tüske sorszámát!")
    end
end)

function showSpikes()
    local px, py, pz = getElementPosition(localPlayer)
    for k, v in pairs(tusik) do
        if isElementOnScreen(v) then
            local ox, oy, oz = getElementPosition(v)
            local dis = getDistanceBetweenPoints3D(ox, oy, oz, px, py, pz)
            if dis < 15 then
                local lx, ly = getScreenFromWorldPosition(ox, oy, oz+1)
                if lx and ly then
                    dxDrawText("Spike id:"..k, lx, ly, lx + 20 , ly + 20, tocolor ( 255, 255, 255, 255), 1, 1, fontName)
                end
            end
        end
    end
end

addEvent("onSpikeAtad", true)
addEventHandler("onSpikeAtad", root, function(spikes)
    tusik = spikes
    local fasz = tusik[1]
    local x,y,z = getElementPosition(fasz)
    local px, py = getElementPosition(localPlayer)
    local x0, y0, z0, x1, y1, z1 = getElementBoundingBox(fasz)
end)

function onClientColShapeHit( theElement, matchingDimension )
    if getElementType(theElement) == "vehicle" and bigCols[source] and matchingDimension then
        kocsikInCol[theElement] = bigCols[source]
        addEventHandler("onClientRender", root, kerekPukkan)
    end
end
addEventHandler("onClientColShapeHit", root, onClientColShapeHit)

function onClientColShapeLeft( theElement, matchingDimension )
    if getElementType(theElement) == "vehicle" and bigCols[source] and matchingDimension then
        kocsikInCol[theElement] = nil
        if getElementsWithinColShape(source, "vehicle") == {} then
            removeEventHandler("onClientRender", root, kerekPukkan)
        end
    end
end
addEventHandler("onClientColShapeLeave", root, onClientColShapeLeft)

function kerekPukkan()
    for k,v in pairs(kocsikInCol) do
        local lf, lb, rf, rb = getVehicleWheelStates(k)
        if lf == 0 or lb == 0 or rf == 0 or rb == 0 then
            if lf < 1 then
                local x, y, z = getVehicleComponentPosition(k, "wheel_lf_dummy", "world")
                if isInsideColShape(v, x, y, z) then lf = 1 end
            end
            if lb < 1 then
                local x, y, z = getVehicleComponentPosition(k, "wheel_lb_dummy", "world")
                if isInsideColShape(v, x, y, z) then lb = 1 end
            end
            if rf < 1 then
                local x, y, z = getVehicleComponentPosition(k, "wheel_rf_dummy", "world")
                if isInsideColShape(v, x, y, z) then rf = 1 end
            end
            if rb < 1 then
                local x, y, z = getVehicleComponentPosition(k, "wheel_rb_dummy", "world")
                if isInsideColShape(v, x, y, z) then rb = 1 end
            end
            outputChatBox("Ok")
            setVehicleWheelStates(k, lf, lb, rf, rb)
        end
    end
end

-- addEventHandler("onClientVehicleCollision", root, function(collider, damageImpulseMag, bodyPart, x, y, z, nx, ny, nz)
--     outputChatBox(inspect(bodyPart))
-- end)

-- addEventHandler ( "onClientResourceStart", resourceRoot, function ()
--     dff = engineLoadDFF ( "files/models/spike.dff" );
-- 	engineReplaceModel ( dff, 2892 );
--     col_floors = engineLoadCOL ( "files/models/spike.col" )
--     engineReplaceCOL ( col_floors, 2892 )
-- end);
