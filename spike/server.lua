local spikes = {}

addEvent("onSpikeLerak", true)
addEventHandler("onSpikeLerak", root, function(x, y, z, rx, ry, rz)
    spike = createObject(2892, x, y, z)
    setElementRotation(spike, rx, ry, rz)
    setElementFrozen(spike, true)
    table.insert(spikes, spike)
    triggerClientEvent("onSpikeAtad", source, spikes)
end)

addEvent("onShowSpike", true)
addEventHandler("onShowSpike", root, function()
    triggerClientEvent("onSpikeAtad", source, spikes)
end)

addEvent("onDelSpike", true)
addEventHandler("onDelSpike", root, function(id)
    destroyElement(spikes[id])
    spikes[id] = nil
    triggerClientEvent("onSpikeAtad", source, spikes)
end)